angular.module('aguagenteclient')
  .config(function ($stateProvider) {

    $stateProvider

      .state('app.guest', {
        cache: false,
        url: '/guest',
        abstract: true,
        views: {
          'menuContent@app': {
            template: '<ion-view><ion-nav-view name="clientContent"></ion-nav-view></ion-view>',
            controller: 'GuestMainCtrl'
          }
        }
      })

      .state('app.guest.login', {
        url: '/login',
        views: {
          'clientContent': {
            templateUrl: 'app/guest/login/login.html',
            controller: 'LoginController'
          }
        }
      })

      .state('app.guest.home', {
        cache: false,
        url: '/home',
        views: {
          'clientContent': {
            templateUrl: 'app/guest/home/home.html',
            controller: 'HomeCtrl'
          }
        }
      })

      .state('app.guest.beneficios', {
        cache: false,
        url: '/beneficios',
        views: {
          'clientContent': {
            templateUrl: 'app/guest/beneficios/beneficios.html'
          }
        }
      })

      .state('app.guest.report', {
        cache: false,
        url: '/report',
        views: {
          'clientContent': {
            templateUrl: 'app/guest/report/report.html',
            controller: 'ReportCtrl'
          }
        }
      })

      .state('app.guest.aguagratis', {
        cache: false,
        url: '/aguagratis',
        views: {
          'clientContent': {
            templateUrl: 'app/guest/aguagratis/aguagratis.html',
            controller: 'AguagratisCtrl'
          }
        }
      })

      .state('app.guest.media', {
        cache: false,
        url: '/media',
        views: {
          'clientContent': {
            templateUrl: 'app/guest/media/media.html',
            controller: 'MediaCtrl'
          }
        }
      })

      .state('app.guest.single', {
        cache: false,
        url: '/media/:mediaid',
        views: {
          'clientContent': {
            templateUrl: 'app/guest/mediaSingle/single.html',
            controller: 'SingleCtrl'
          }
        }
      })

  });
