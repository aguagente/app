function LoginController($scope, $rootScope, $state, $ionicPopup, $ionicHistory, $ionicModal, AuthService, SessionService, $cordovaFacebook) {

  if (AuthService.isRemember()) {

    var route = null;

    if (AuthService.isAuthenticated())
      route = 'app.client.welcome';

    else if (AuthService.isAuthenticatedGuest())
      route = 'app.seller.welcome';

    if (route != null)
      SessionService.update().then(function (data) {
        $state.go(route, {}, {reload: true});
      }, function (err) {
        //$state.go(route, {}, {reload: true});
      });


  }

  //route = 'app.guest.welcome';
  //$state.go(route, {}, { reload: true });

  $scope.popUp = function (msj, success, callback) {
    var alertPopup = $ionicPopup.alert({
      title: success ? 'Correcto!' : 'Oops!',
      template: msj,
      buttons: [{
        'text': 'Aceptar',
        'type': success ? 'button-balanced' : 'button-assertive',
        'onTap': callback
      }]
    });
  };

  $scope.data = {
    //'username' : 'jav0874@gmail.com',
    //'password' : 'qwerty'
  };

  $scope.remember = true;

  $scope.changeRemember = function () {
    $scope.remember = !$scope.remember;
  };

  $scope.ingresar = function () {
    if (!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test($scope.data.username)) {
      $scope.popUp("Email invalido", false, null);
    } else if (!$scope.data.password || $scope.data.password.length < 6) {
      $scope.popUp("Password invalido", false, null);
    } else
      AuthService
        .login({
          'email': $scope.data.username,
          'password': $scope.data.password,
          'remember': $scope.remember
        })
        .then(function (response) {
          $scope.data = {};
          $ionicHistory.clearCache().then(function () {
            $ionicHistory.clearHistory();
            if (SessionService.getClientStatus() == 'invalid') {
              $state.go('app.client.editar-contrato');
            }else{
              $state.go('app.guest.home');
            }
          });
        }, function (err) {
          AuthService.getToken();
          $scope.popUp(err, false, null);
        });

  };

  $ionicModal.fromTemplateUrl('app/modals/contacto.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.contactModal = modal;
  });

  $scope.contacto = function (form) {
    if (!form.$valid) return;
    AuthService
      .unregistered($scope.data.contact)
      .then(function (data) {

        $scope.data.contact = {};
        form.$setPristine();
        $scope.popUp("Mensaje enviado correctamente", true, function () {
          $scope.closeModal();
        });

      }, function (err) {
        console.log(err);
        $scope.popUp("Ocurrio un error al enviar el mensaje, por favor vuelve a intentarlo", false, null);

      });
  };

  $scope.openModal = function () {
    $scope.contactModal.show();
  };

  $scope.closeModal = function () {
    $scope.contactModal.hide();
  };

  $scope.$on('$destroy', function () {
    $scope.contactModal.remove();
  });

  $scope.forget = function () {

    var myPopup = $ionicPopup.show({
      template: '<input type="email" ng-model="data.forget">',
      title: 'Restaurar Password',
      subTitle: 'Ingresa tu email',
      scope: $scope,
      buttons: [
        {text: 'Cancelar'},
        {
          text: '<b>Enviar</b>',
          type: 'button-positive',
          onTap: function (e) {
            if (!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test($scope.data.forget)) {
              $scope.popUp("Email invalido", false, function () {
                $scope.forget();
              });
            } else {

              AuthService
                .forget($scope.data.forget)
                .then(function (response) {
                  $scope.data.forget = '';
                  $scope.popUp("Mensaje enviado correctamente", true, null);
                }, function (response) {
                  $scope.popUp("Ocurrio un error al enviar el correo, por favor vuelve a intentarlo", false, null);
                });

            }
          }
        }
      ]
    });

  };

  $scope.referir = function () {

    var myPopup = $ionicPopup.show({
      template: '<input type="email" ng-model="data.email">',
      title: 'Verificar vendedor',
      subTitle: 'Ingresa el email del vendedor',
      scope: $scope,
      buttons: [
        {text: 'Cancelar'},
        {
          text: '<b>Enviar</b>',
          type: 'button-positive',
          onTap: function (e) {
            if (!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test($scope.data.email)) {
              $scope.popUp("Email invalido", false, function () {
                $scope.referir();
              });
            } else {

              AuthService
                .getClientByEmail($scope.data.email)
                .then(function (response) {

                  $scope.data.email = '';

                  if (response.success && response.response.length) {

                    SessionService.setGuest(response.response[0]);
                    SessionService.setRemember(angular.copy($scope.remember));
                    $state.go('app.seller.welcome', {}, {reload: true});

                  } else {

                    $scope.popUp("El email que ingresaste no es vendedor", false, function () {
                      $scope.referir();
                    });

                  }

                }, function (response) {
                  console.log(response);
                  $scope.popUp("Ocurrio un error al verificar el email, por favor vuelve a intentarlo", false, null);
                });

            }
          }
        }
      ]
    });

  };

  $scope.loginFacebook = function (accessToken) {

    AuthService
      .login({
        'token': accessToken,
        'remember': true
      }, true)
      .then(function (response) {

        $rootScope.$broadcast('loading:hide');
        $scope.data = {};
        $ionicHistory.clearCache().then(function () {
          $ionicHistory.clearHistory();
          $state.go('app.guest.home');
        });

      }, function (err) {

        $rootScope.$broadcast('loading:hide');
        $scope.popUp(err, false, null);

      });

  };

  $scope.facebookLogin = function () {

    $cordovaFacebook.login(["public_profile", "email"])
      .then(function (success) {

        $cordovaFacebook.getAccessToken()
          .then(function (token) {
            $scope.loginFacebook(token);
          }, function (error) {
          });

      }, function (error) {
      });

  };

  $scope.ingresarFacebook = function () {

    $cordovaFacebook.getLoginStatus()
      .then(function (success) {
        if (success.status == "connected") {
          $scope.loginFacebook(success.authResponse.accessToken);
        } else {
          $scope.facebookLogin();
        }
      }, function (error) {
        $scope.facebookLogin();
      });

  };

}

LoginController.$inject = ['$scope', '$rootScope', '$state', '$ionicPopup', '$ionicHistory', '$ionicModal', 'AuthService', 'SessionService', '$cordovaFacebook'];
