function MediaCtrl(
    $scope, MediaGallery, $cordovaSocialSharing) {

	$scope.textLoad = 'Cargando medios...';

	MediaGallery.get().then(function(response){
        $scope.$parent.medios = response;
        $scope.textLoad = 'No se encontraron medios';
    }, function(err){
        $scope.textLoad = 'No se encontraron medios';
    });

    $scope.shareEmail = function(item){

        var user = $scope.currentUser.getUser();

        var es_url = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/.test(item.text);
        var img = (/\.(gif|jpg|jpeg|tiff|png)$/i).test(item.image);
        var link = es_url ? item.text : (img ? item.image : '');
        var title = item.title != '' ? 'Ver multimedia '+item.title : 'Ver multimedia';

        var properties = {
            to: user.email,
            cc: '',
            bcc: '',
            attachments: [],
            subject: item.title,
            body: '<div style="text-align:center;"><img src="http://www.aguagente.dreamhosters.com/wp-content/uploads/2015/11/logocentrado250.png" style="width:50%; margin:0px auto;" /> <br/><br/> Hola, te comparto la siguiente información. <br/><a href="'+link+'">'+title+'</a></div>',
            isHtml: true
        };

        cordova.plugins.email.open(properties, function () {
            console.log('email view dismissed');
        }, this);

    };

}

MediaCtrl.$inject = [
    "$scope", "MediaGallery", "$cordovaSocialSharing"
];
