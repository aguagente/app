function AguagratisCtrl($scope, API) {

  $scope.link = API.FACE+($scope.currentUser.getIdClient() !== null ? '/'+$scope.currentUser.getIdClient() : '');

  var _playOnopenLocalStorage = window.localStorage['playvideoonopen'] ? JSON.parse(window.localStorage['playvideoonopen']):true;
  var _chck = _playOnopenLocalStorage;
  
  if (typeof _playOnopenLocalStorage !== "boolean"){
    _chck = true;
  }
  $scope.playVideoOnOpen = {
    checked: _chck
  }

  $scope.toggleVideoOnOpen = function(){
    window.localStorage['playvideoonopen'] = $scope.playVideoOnOpen.checked;
    $scope.playVideoOnOpen                 = { checked: $scope.playVideoOnOpen.checked};
  };
}

AguagratisCtrl.$inject = [
  "$scope", "API"
];