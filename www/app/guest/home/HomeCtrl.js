function HomeCtrl($scope, $rootScope, $ionicPopup, MediaGallery, $cordovaSocialSharing) {
  $scope.medios = [];
  $scope.referidos = 0;

  angular.element(document).ready(function () {
    MediaGallery.get().then(function (response) {
      if ($scope.$parent) {
        $scope.$parent.medios = response;
        $scope.medios = $scope.getMediaList(response);
      }
    });
  });



  $scope.currentUser.getReferidos().then(
    function (response) {
      if (response.response.referidos) {
        $scope.referidos = response.response.referidos;
      }
    }
  );

  $scope.closeVideo = function () {
    $rootScope.popupVideoHide = true;
  };

  $scope.src = 'aguagente_sub';
  $scope.subtitles = false;

  /*$scope.reproducirVideo = function(){
    VideoPlayer.play("file:///android_asset/www/img/seller/"+$scope.src+".mp4");
  };*/

  $scope.toggleVideo = function () {
    $scope.src = ($scope.src == 'aguagente' ? 'aguagente_sub' : 'aguagente');
  };

  $scope.$watch('subtitles', function (newVal, OldVal) {
    $scope.toggleVideo();
  });

  $scope.getMediaList = function () {
    var _medios = $scope.$parent.medios,
      _ready = [];

    angular.forEach(_medios, function (value, key) {
      var es_url = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/.test(value.text),
        img = (/\.(gif|jpg|jpeg|tiff|png)$/i).test(value.image),
        _item = {
          id: key,
          title: value.title,
          link: value.text
        };

      if (es_url) {
        _item.isImg = false;
        _item.video = (value.text.split('/')[3]).replace("watch?v=", "");
      }

      if (value.image != null) {
        _item.isImg = true;
        _item.img = value.image;
      }

      this.push(_item);
    }, _ready);
    return _ready;
  };

  $scope.compartir = function (item) {
    $cordovaSocialSharing
      .share(
        '¡Mira los beneficios de tener el sistema Aguagente!\n #AdiosGarrafones\n ' + item.link, //message
        'Mira los beneficios de tener el sistema Aguagente', //subject
        ['http://i3.ytimg.com/vi/' + item.video + '/maxresdefault.jpg'], //media
        API.FACE + '/' + $scope.currentUser.getIdClient()   //link
      )
      .then(function (result) {

      }, function (err) {
        console.log(err);
        $ionicPopup.alert({
          title: 'Oops!',
          template: 'Ocurrio un error al compartir',
          buttons: [{
            'text': 'Aceptar',
            'type': 'button-assertive'
          }]
        });
      });
  };
}

HomeCtrl.$inject = [
  "$scope", "$rootScope", "$ionicPopup", "MediaGallery", "$cordovaSocialSharing"
];
