function SingleCtrl($scope, $http, $stateParams, $cordovaSocialSharing) {

  var medio = $scope.$parent.medios[$stateParams.mediaid];
  var es_url = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/.test(medio.text),
    img = (/\.(gif|jpg|jpeg|tiff|png)$/i).test(medio.image),
    _title = medio.title.split(' - '),
    _item = {
      header: _title[0],
      description: _title[1],
      link: medio.text,
      title: _title[0]
    };

  if (es_url) {
    _item.isImg = false;
    _item.video = (medio.text.split('/')[3]).replace("watch?v=", "");
  }

  if (medio.image != null) {
    _item.isImg = true;
    _item.img = medio.image;
  }

  $scope.medio = _item;

  $scope.compartir = function () {
    $cordovaSocialSharing
      //.share($scope.medio.header, '¡Mira los beneficios de tener el sistema Aguagente!\n #AdiosGarrafones', $scope.medio.video, $scope.medio.link)
      .share(
        '¡Mira los beneficios de tener el sistema Aguagente!\n #AdiosGarrafones\n '+$scope.medio.link, //message
        'Mira los beneficios de tener el sistema Aguagente', //subject
        ['http://i3.ytimg.com/vi/'+$scope.medio.video+'/maxresdefault.jpg'], //media
        API.FACE + '/' + $scope.currentUser.getIdClient()   //link
      )
      .then(function (result) {

      }, function (err) {
        console.log(err);
        $ionicPopup.alert({
          title: 'Oops!',
          template: 'Ocurrio un error al compartir',
          buttons: [{
            'text': 'Aceptar',
            'type': 'button-assertive'
          }]
        });
      });
  };
}

SingleCtrl.$inject = [
  "$scope", "$http", "$stateParams", "$cordovaSocialSharing"
];
