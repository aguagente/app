function GuestMainCtrl($scope, $ionicPopup, API, $cordovaSocialSharing, $ionicModal, $ionicActionSheet, $cordovaClipboard, DashboardChart,AuthService) {

  AuthService.checkAppVersion().then(function (response) {

  },function (error) {
    console.log(error);
  })

  $scope.setMenuType("guest");
  //$scope.medios = [];
  $scope.textLoad = 'Cargando medios...';
  $scope.modalItem = null;

  $scope.shareWhatsApp = function (item) {
    var _msg = item.title + '\n' + item.link + '\n\rRegistrate en: ' + API.FACE;
    if ($scope.currentUser.getIdClient() !== null) {
      //_msg += '?r=' + $scope.currentUser.getIdClient();
      _msg += '/' + $scope.currentUser.getIdClient();
    }

    $cordovaSocialSharing
      .shareViaWhatsApp(
        _msg,
        null,//(img ? item.image : null),
        null//item.link//(img ? null : API.FACE+'/'+$scope.currentUser.getIdClient())
      )
      .then(function (result) {
        // Success!
      }, function (err) {

        $ionicPopup.alert({
          title: 'Oops!',
          template: 'Ocurrio un error al compartir por whatsapp',
          buttons: [{
            'text': 'Aceptar',
            'type': 'button-assertive'
          }]
        });
      });

  };

  $scope.shareFacebook = function (item) {
    var _msg = item.title + '\n #AdiosGarrafones\r\n ' + item.link + '\n\rRegistrate en: ' + API.FACE;
    //var _msg = '\n #AdiosGarrafones\r\n ' + item.link + '\n\rRegistrate en: ' + API.FACE + '/';
    if ($scope.currentUser.getIdClient() !== null) {
      _msg += '/' + $scope.currentUser.getIdClient();
    }
    $cordovaSocialSharing
    //.shareViaFacebookWithPasteMessageHint(
      .shareViaFacebook(
        _msg, //message
        'Mira los beneficios de tener el sistema Aguagente', //subject
        //['http://i3.ytimg.com/vi/' + item.video + '/maxresdefault.jpg'], //media
        null,
        null//link
      )
      .then(function (result) {

      }, function (err) {
        //console.log(err);
        $ionicPopup.alert({
          title: 'Correcto!',
          template: 'En un momento se abrirá la ventana para compartir en Facebook.',
          buttons: [{
            'text': 'Aceptar',
            'type': 'button-balanced'
          }]
        });
      });

    /*
        var opc = {
            method: "feed",
            href: item.link,
            name: 'Aguagente',
            //message: item.title,
            message: 'message',
            caption: 'caption',
            description: 'description',
        };

        facebookConnectPlugin.showDialog(opc, function(res){
            $ionicPopup.alert({
                title: 'Correcto!',
                template: 'Gracias por compartir en facebook',
                buttons: [{
                    'text': 'Aceptar',
                    'type': 'button-balanced'
                }]
            });

        }, function(){
            $ionicPopup.alert({
                    title: 'Oops!',
                    template: 'Ocurrio un error al compartir por facebook',
                    buttons: [{
                        'text' : 'Aceptar',
                        'type' : 'button-assertive'
                    }]
                });

        });
    */

  };

  $ionicModal.fromTemplateUrl('app/modals/postFacebook.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.postModalFace = modal;
  });

  $scope.copyMsgForFacebook = function (item) {
    var _msg = item.title + '\n #AdiosGarrafones\r\n ' + item.link + '\n\rRegistrate en: ' + API.FACE;
    if ($scope.currentUser.getIdClient() !== null) {
      //_msg += '?r=' + $scope.currentUser.getIdClient();
      _msg += '/' + $scope.currentUser.getIdClient();
    }

    $cordovaClipboard
      .copy(_msg)
      .then(function () {
        var _alert = $ionicPopup.alert({
          title: 'Exito',
          template: '¡El texto se ha copiado!',
          buttons: [{
            'text': 'Aceptar',
            'type': 'button-balanced'
          }]
        });
      }, function () {
        // error
      });
  };

  $scope.copyMsgGeneral = function () {
    var _msg = API.FACE;
    if ($scope.currentUser.getIdClient() !== null) {
      //_msg += '?r=' + $scope.currentUser.getIdClient();
      _msg += '/' + $scope.currentUser.getIdClient();
    }

    $cordovaClipboard
      .copy(_msg)
      .then(function () {
        var _alert = $ionicPopup.alert({
          title: 'Exito',
          template: '¡El texto se ha copiado!',
          buttons: [{
            'text': 'Aceptar',
            'type': 'button-balanced'
          }]
        });
      }, function () {
        // error
      });
  };

  $scope.openModalFace = function (item) {
    $scope.modalItem = item;
    $scope.postModalFace.show();
  };

  $scope.closeModalFace = function () {
    $scope.postModalFace.hide();
    $scope.modalItem = null;
  };

  $scope.shareGeneral = function (item) {
    var _msg = item.title + '\n #AdiosGarrafones\r\n ' + item.link + '\n\rRegistrate en: ' + API.FACE;
    if ($scope.currentUser.getIdClient() !== null) {
      //_msg += '?r=' + $scope.currentUser.getIdClient();
      _msg += '/' + $scope.currentUser.getIdClient();
    }
    $cordovaSocialSharing
      .share(
        _msg, //message
        'Mira los beneficios de tener el sistema Aguagente', //subject
        //['http://i3.ytimg.com/vi/' + item.video + '/maxresdefault.jpg'], //media
        null,
        null//link
      )
      .then(function (result) {
      }, function (err) {
        $ionicPopup.alert({
          title: 'Oops!',
          template: 'Ocurrio un error al compartir',
          buttons: [{
            'text': 'Aceptar',
            'type': 'button-assertive'
          }]
        });
      });
  };

  $scope.openActionsheet = function (item) {
    DashboardChart.create({type: 1}).then(function (response) {

    });
    if ($scope.currentUser.getIdClient() === null) {
      var _alert = $ionicPopup.alert({
        title: 'Un momento...',
        template: 'No has iniciado sesión, por lo que los enlaces que compartas no estarán ligados a tu usuario',
        buttons: [{
          'text': 'Aceptar',
          'type': 'button-balanced'
        }]
      });

      _alert.then(function (res) {
        $scope.openActionsheetFunction(item);
      });
    } else {
      $scope.openActionsheetFunction(item);
    }
  };

  $scope.openActionsheetFunction = function (item) {
    // Show the action sheet
    var hideSheet = $ionicActionSheet.show({
      buttons: [
        {text: '<i class="icon ion-social-facebook"></i> Facebook'},
        {text: '<i class="icon ion-social-whatsapp"></i> WhatsApp'},
        {text: '<i class="icon ion-android-share-alt"></i> Otros'}
      ],
      titleText: 'Compartir',
      cancelText: 'Cancelar',
      cancel: function () {
        // add cancel code..
      },
      buttonClicked: function (index) {
        switch (index) {
          case 0:
            DashboardChart.create({type: 2}).then(function (response) {

            });
            hideSheet();
            $scope.openModalFace(item);
            break;
          case 1:
            DashboardChart.create({type: 3}).then(function (response) {

            });
            $scope.shareWhatsApp(item);
            break;
          case 2:
            DashboardChart.create({type: 4}).then(function (response) {

            });
            $scope.shareGeneral(item);
            break;
        }
      }
    });


  };


  //DEPRECATED
  $scope.referirWhatsApp = function () {

    if ($scope.medios.length > 0) {

      $scope.shareWhatsApp($scope.medios[0]);

    } else
      $cordovaSocialSharing
        .shareViaWhatsApp('Unete al mundo de Aguagente!', null, API.FACE + '/' + $scope.currentUser.getIdClient())
        .then(function (result) {
        }, function (err) {

          $ionicPopup.alert({
            title: 'Oops!',
            template: 'Ocurrio un error al compartir por whatsapp',
            buttons: [{
              'text': 'Aceptar',
              'type': 'button-assertive'
            }]
          });
        });

  };

  //DEPRECATED
  $scope.referirFace = function () {

    if ($scope.medios.length > 0) {

      $scope.openMedia($scope.medios[0]);

    } else
      $scope.referirFacebook();

  };

  //DEPRECATED
  $scope.referirFacebook = function () {
    facebookConnectPlugin.showDialog({
      method: "share",
      href: API.FACE + '/' + $scope.currentUser.getIdClient(),
      caption: 'Regístrate en Aguagente',
      description: 'Entra, conoce y disfruta el mundo de aguagente',
      picture: 'https://panelaguagente.xyz/registrarme.png'
    }, function () {

      $ionicPopup.alert({
        title: 'Correcto!',
        template: 'Gracias por compartir en facebook',
        buttons: [{
          'text': 'Aceptar',
          'type': 'button-balanced'
        }]
      });

    }, function () {

      $ionicPopup.alert({
        title: 'Oops!',
        template: 'Ocurrio un error al compartir por facebook',
        buttons: [{
          'text': 'Aceptar',
          'type': 'button-assertive'
        }]
      });

    });

  };

}

GuestMainCtrl.$inject = [
  '$scope', '$ionicPopup', 'API', '$cordovaSocialSharing', '$ionicModal', '$ionicActionSheet', '$cordovaClipboard', 'DashboardChart', 'AuthService'
];
