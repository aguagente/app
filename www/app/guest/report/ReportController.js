function ReportCtrl($scope, $ionicPopup, $state, $ionicHistory, Notificacion) {

  $scope.subjects = [
    {
      id: 1,
      name: 'Problema con la App'
    },
    {
      id: 2,
      name: 'Soporte'
    },

    {
      id: 3,
      name: 'Comentarios'
    },

    {
      id: 4,
      name: 'General'
    }
  ];

  $scope.data = {
    name: '',
    asunto: '',
    email: '',
    msg: ''
  };

  $scope.sendMessage = function (form) {
    if (!form.$valid) return;

    Notificacion
      .report({
        'nombre': $scope.data.name,
        'asunto': $scope.data.asunto,
        'email': $scope.data.email,
        'msg': $scope.data.msg
      })
      .then(function (response) {
        $scope.data = {};

        $ionicPopup.alert({
          title: 'Reporte enviado',
          template: "Su reporte ha sido enviado, en breve recibirá nuestra respuesta.<br> Gracias.",
          buttons: [{
            'text': 'Aceptar',
            'type': 'button-balanced',
            'onTap': function (e) {
              $ionicHistory.currentView($ionicHistory.backView());
              $state.go('app.guest.home', {}, {location: 'replace'});
            }
          }]
        });


      }, function (err) {

      });
  };
}

ReportCtrl.$inject = [
  "$scope", "$ionicPopup", "$state", "$ionicHistory", "Notificacion"
];
