function PagoPreviewCtrl($scope,$filter) {

    $scope.group = $scope.currentUser.getGroupSeller();

    $scope.extras = [];
    $scope.total_extras = 0;
    $scope.resp_social = 0;
    $scope.resp_mensual = 0;

    $scope.extras.push({
        "price" : $scope.contractData.material.price,
        "name" : $scope.contractData.material.name
    });

    $scope.total_extras += parseFloat($scope.contractData.material.price);

    angular.forEach( $scope.contractData.elements, function(value, key){

        $scope.extras.push({
            "price" : value.price,
            "name" : value.name
        });

        $scope.total_extras += parseFloat(value.price);
        
    });

    if($scope.contractData.responsabilidad){
        $scope.resp_social = ($scope.total_extras / 1.16) * 0.007;
        $scope.resp_mensual = ( ($scope.vendedor.sign_into.monthly_fee / 100) / 1.16) * 0.007;
    }

    $scope.total_extras += $scope.resp_social;

}

PagoPreviewCtrl.$inject = [
    '$scope','$filter'
];