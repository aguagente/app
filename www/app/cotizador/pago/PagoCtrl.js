function PagoCtrl($scope, $state, Payment, $ionicPopup, $ionicPopover) {
	$scope.brand_name;
	$scope.tarjeta = angular.copy($scope.contractData.credit_card);
	$scope.fee = $scope.tarjeta.bank ? $scope.fees[$scope.relationCard[$scope.tarjeta.bank]] : null;

	$scope.openPopover = function ($event, templateName) {
		// Init popover on load
		$ionicPopover.fromTemplateUrl(templateName, {
			scope: $scope,
		}).then(function (popover) {
			$scope.popover = popover;
			$scope.popover.show($event);
		});
	};

	$scope.closePopover = function () {
		$scope.popover.hide();
	};
	//Cleanup the popover when we're done with it!
	$scope.$on('$destroy', function () {
		if ($scope.popover){
		  $scope.popover.remove();
    }
	});

	$scope.setMonth = function(month) {
		$scope.tarjeta.expiration_month = month;
		$scope.closePopover();
	}

	$scope.setYear = function (year) {
		$scope.tarjeta.expiration_year = year;
		$scope.closePopover();
	}

	$scope.setBrand = function (brand) {
		switch (brand) {
			case 'visa':
				$scope.brand_name = ' VISA';
				break;
			case 'mastercard':
				$scope.brand_name = ' MASTERCARD';
				break;
			case 'amex':
				$scope.brand_name = ' AMEX';
				break;
		}
		$scope.tarjeta.bank = brand;
		$scope.changeBrand();
		$scope.closePopover();
	}

	$scope.setForma = function (key) {
		$scope.contractData.monthly_installments = key;
		$scope.changeFormaPago();
		$scope.closePopover();
	}

	Payment.getFees().then(function(response){

		$scope.$parent.fees = response;
		$scope.fee = $scope.tarjeta.bank ? $scope.fees[$scope.relationCard[$scope.tarjeta.bank]] : null;

    });

	$scope.validarTarjeta = function(data){
		if( !Conekta.card.validateNumber(data.card_number) ) return 'Número de tarjeta invalido';
		if( !Conekta.card.validateCVC(data.cvc) ) return 'CVC incorrecto';
		if( !Conekta.card.validateExpirationDate(data.expiration_month,data.expiration_year) ) return 'Fecha de vencimiento invalida';
		if( !data.bank ) return 'Tipo de tarjeta obligatorio';
		if( Conekta.card.getBrand( data.card_number.toString() ) != data.bank ) return 'Tipo de tarjeta incorrecto';
		return false;
	};

	$scope.changeBrand = function(){
		if($scope.tarjeta.bank){

			$scope.fee = $scope.fees[$scope.relationCard[$scope.tarjeta.bank]];
			$scope.contractData.fee_installments = parseFloat($scope.fees[$scope.relationCard[$scope.tarjeta.bank]][$scope.contractData.monthly_installments]);

		}
		else
			$scope.fee = null;
	};

	$scope.changeFormaPago = function(){

		$scope.contractData.fee_installments = parseFloat($scope.fees[$scope.relationCard[$scope.tarjeta.bank]][$scope.contractData.monthly_installments]);

	};


	$scope.enviar = function(){

		var error = $scope.validarTarjeta($scope.tarjeta);

		if(error){
			$scope.popUp(error,false,null);
		}
		else{

			$scope.contractData.credit_card = angular.copy($scope.tarjeta);
			if($state.includes('app.seller'))
				$state.go("app.seller.preview");
			else
				$state.go("app.cotizador.preview");

		}
		return;
	};

	$scope.enviarNoCard = function () {

		if ($state.includes('app.seller'))
			$state.go("app.seller.preview");
		else
			$state.go("app.cotizador.preview");
		//return;
	};

}

PagoCtrl.$inject = [
	'$scope', '$state', 'Payment', '$ionicPopup', '$ionicPopover'
];
