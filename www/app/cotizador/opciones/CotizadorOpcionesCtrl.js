function CotizadorOpcionesCtrl($scope, $filter, Categories, Elements) {

  //$scope.group = $scope.currentUser.getGroupSeller();
  /*console.log($scope.group);
  console.log($scope.currentUser);
  console.log($scope.vendedor);*/
  $scope.group = $scope.vendedor;
  $scope.deposito = parseFloat($scope.vendedor.sign_into.deposit) / 100;


  if ($scope.contractData.elems && $scope.contractData.elems.length) {

    angular.forEach($scope.contractData.elems, function (value, key) {
      if (value.category_name != 'Material') {
        var elems = $filter('filter')($scope.$parent.categories, {'name': value.category_name})[0];
        var elem = $filter('filter')(elems.elements, {'name': value.element_name});
        if (elem)
          $scope.contractData.elements[elem[0].id_categories_elements] = elem[0];
      }
    });
  }

  $scope.changeMaterial = function (elem, $event) {
    if ($event.target.checked) {
      $scope.contractData.material = elem;
    } else {
      $scope.contractData.material = {};
    }
  };

  $scope.toggleResponsabilidad = function () {
    $scope.contractData.responsabilidad = !$scope.contractData.responsabilidad;
  };

  $scope.toggleExtra = function (extra) {

    if ($scope.contractData.elements[extra.id_categories_elements]) {
      delete $scope.contractData.elements[extra.id_categories_elements];
    } else {
      $scope.contractData.elements[extra.id_categories_elements] = extra;
    }
  };

  $scope.clickExtra = function ($event, extra) {

    if (angular.element(event.target).hasClass('ion-image')) {
      $scope.image = 'data:' + extra.type + ';base64,' + extra.image;
      $scope.modal.show();
    } else {
      $scope.toggleExtra(extra);
    }
  };

  Categories.getAll().then(function (response) {

    $scope.$parent.categories = response;

    $scope.material = $filter('filter')(response, {'name': 'Material'})[0];
    //console.log(response);
    if ($scope.material) {
      var acero = $filter('filter')($scope.material.elements, {'name': 'Acero inoxidable'})[0];
      if (acero) {
        acero.price = parseFloat($scope.vendedor.sign_into.installation_fee) / 100;
      }
    }

    var element = $scope.material ? $scope.material.elements[0] : {};
    if (Object.keys($scope.contractData.material).length > 0) {

      if ($scope.contractData.material.id_categories_elements)
        var elegido = $filter('filter')($scope.material.elements, {'id_categories_elements': $scope.contractData.material.id_categories_elements});
      else {
        var elegido = $filter('filter')($scope.material.elements, {'name': $scope.contractData.material.element_name});

      }
      element = elegido ? elegido[0] : element;

    }
    $scope.changeMaterial(element, {target: {checked: true}});

  });

}

CotizadorOpcionesCtrl.$inject = [
  '$scope', '$filter', 'Categories', 'Elements'
];
