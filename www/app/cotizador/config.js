angular.module('aguagenteclient')
.config(function($stateProvider){
	$stateProvider

		.state('app.cotizador', {
			cache    : false,
			url: '/cotizador',
			abstract : true,
			views    : {
				'menuContent@app' : {
					template   : '<ion-view><ion-nav-view name="guestContent"></ion-nav-view></ion-view>',
					controller: 'CotizadorMainCtrl'
				}
			}
		})

		.state('app.cotizador.start', {
			cache: false,
			url: '/start',
			views: {
				'guestContent': {
					templateUrl: 'app/cotizador/start/start.html'
				}
			}
		})

		.state('app.cotizador.opciones', {
			cache: false,
			url: '/opciones',
			views: {
				'guestContent': {
					templateUrl: 'app/cotizador/opciones/opciones.html',
					controller: 'CotizadorOpcionesCtrl'
				}
			}
		})

		.state('app.cotizador.datos', {
			cache : false,
			url: '/datos',
			views : {
				'guestContent' : {
					templateUrl: 'app/cotizador/datosCliente/datos.html'
				}
			}
		})

		.state('app.cotizador.pago', {
			cache: false,
			url: '/pago',
			views: {
				'guestContent': {
					templateUrl: 'app/cotizador/pago/pago.html',
					controller: 'PagoCtrl'
				}
			}
		})

		.state('app.cotizador.preview', {
			cache: false,
			url: '/preview',
			views: {
				'guestContent': {
					templateUrl: 'app/cotizador/preview/preview.html',
					controller: 'PagoPreviewCtrl'
				}
			}
		})
	
})