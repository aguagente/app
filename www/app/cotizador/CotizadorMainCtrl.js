function CotizadorMainCtrl($scope, $rootScope, $state, $filter, Contracts, Coupon, $ionicPopup, $cordovaCamera, $cordovaActionSheet, AuthService, Imagen) {

  //$scope.setMenuType("guest");

  $scope.categories = [];
  $scope.elements = [];
  $scope.fees = {};
  $scope.vendedor = {};

  $scope.relationCard = {
    "amex": "AMERICAN_EXPRESS",
    "visa": "VISA",
    "mastercard": "MC"
  };

  $scope.onSwipe = function (event) {
    event.preventDefault();
  };

  $scope.totalCurrentContract = 0;
  $scope.contractData = {
    "fecha": $filter('date')(new Date(), 'yyyy-MM-dd'),
    "material": {},
    "elements": {},
    "mensual": 0,
    "responsabilidad": true,
    "monthly_installments": 1,
    "fee_installments": 0,
    "credit_card": {},
    "state": 'Jalisco',
    "cupon": null
  };

  //console.log($scope.currentUser.session);
  if ($scope.currentUser.session.user) {
    $scope.contractData.referido = $scope.currentUser.session.user.email;
  }

  $scope.datosConekta = function (data, name) {
    return {
      "name": name,
      "number": data.card_number,
      "cvc": data.cvc,
      "exp_month": data.expiration_month,
      "exp_year": data.expiration_year
    };
  };

  $scope.contractFotos = [];

  $scope.popUp = function (msj, success, callback) {
    var alertPopup = $ionicPopup.alert({
      title: success ? 'Correcto!' : 'Ooops!',
      template: msj,
      buttons: [{
        'text': 'Aceptar',
        'type': success ? 'button-balanced' : 'button-assertive',
        'onTap': callback
      }]
    });
  };

  $scope.cameraImage = function (index, type) {

    var options = {
      quality: 40,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: type,
      allowEdit: false,
      encodingType: Camera.EncodingType.JPEG,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    $cordovaCamera
      .getPicture(options)
      .then(function (imageData) {
        $scope.contractFotos[index] = 'data:image/jpeg;base64,' + imageData;
      }, function (err) {
        console.log(err);
      });

  };

  $scope.captureImage = function (index, sourceType) {

    var options = {
      title: 'Metodo para obtener la imagen',
      buttonLabels: ['Carrete/Galeria', 'Hacer foto'],
      addCancelButtonWithLabel: 'Cancelar',
      androidEnableCancelButton: true,
      winphoneEnableCancelButton: true
    };

    var types = {
      1: Camera.PictureSourceType.PHOTOLIBRARY,
      2: Camera.PictureSourceType.CAMERA
    };

    $cordovaActionSheet
      .show(options)
      .then(function (btnIndex) {

        if (btnIndex == 1 || btnIndex == 2)
          $scope.cameraImage(index, types[btnIndex]);

      });
  };

  $scope.removeImage = function (index) {
    $scope.contractFotos[index] = null;
  };

  $scope.validateEmail = function () {
    if (!$scope.contractData.referido) {
      $scope.contractData.referido = 'aguagratis@aguagente.com'
    }

    AuthService
      .getClientByEmail($scope.contractData.referido)
      .then(function (response) {

        if (response.success && response.response.length) {
          $scope.vendedor = response.response[0].group;
          $scope.contractData.seller = response.response[0].id_clients;
          $scope.contractData.mensual = $scope.vendedor.monthly_fee / 100;
          //SessionService.setGuest(response.response[0]);

          if ($scope.contractData.cupon === null || $scope.contractData.cupon === "") {
            $scope.isValidCupon = false;
            $scope.descuento = 0;
            $state.go('app.cotizador.opciones');

          } else if ($scope.contractData.cupon !== null) {

            Coupon.get({
              'token': $scope.contractData.cupon
            }).then(function (response) {
              console.log(response);
              console.log($scope.contractData);
              if (response.length > 0) {
                var _level = response[0].level;

                $scope.isValidCupon = true;
                $scope.descuento = Coupon.getDiscount(_level);

                /*switch(_level){
                  case 1:
                      $scope.descuento = -100;
                      break;
                  case 2:
                      $scope.descuento = -100;
                      break;
                  case 3:
                      $scope.descuento = -200;
                      break;
                  case 4:
                      $scope.descuento = -499;
                      break;
                  case 5:
                      $scope.descuento = -799;
                      break;
                  default:
                      $scope.descuento = -999;
                      break;
              }*/

                $state.go('app.cotizador.opciones');
              } else {
                var alertPopup = $ionicPopup.alert({
                  title: 'Oops!',
                  template: 'El cupon no es válido, ingresa otro o elimina el que ingresaste',
                  buttons: [{
                    'text': 'Aceptar',
                    'type': 'button-assertive'
                    /*'onTap' : function(e){
                        $state.go('app.cotizador.datos');
                    }*/
                  }]
                });
              }

            }, function (err) {
              var alertPopup = $ionicPopup.alert({
                title: 'Oops!',
                template: 'El cupon no es válido, ingresa otro o elimina el que ingresaste',
                buttons: [{
                  'text': 'Aceptar',
                  'type': 'button-assertive'
                  /*'onTap' : function(e){
                      $state.go('app.cotizador.datos');
                  }*/
                }]
              });
            });

          } else {
            console.log("no valido");
          }
        } else {
          $scope.popUp("<p class='text-center'>El email que ingresaste no existe.</p><p>Si no te lo sabes, deja este campo en blanco.</p>", false);
        }

      }, function (response) {
        $scope.popUp("Ocurrio un error al verificar el email, por favor vuelve a intentarlo", false, null);
      });
  };

  $scope.saveContract = function () {
    $scope.contractData.fecha = $filter('date')(new Date(), 'yyyy-MM-dd');
    var contrato = angular.copy($scope.contractData);
    contrato.fotos = angular.copy($scope.contractFotos);


    $scope.enviar(contrato);
  };

  $scope.createContract = function () {
    AuthService
      .getClientByEmail($scope.contractData.email)
      .then(function (response) {

        if (response.success && response.response.length) {

          var alertPopup = $ionicPopup.alert({
            title: 'Oops!',
            template: 'El email del contrato ya ha sido previamente registrado',
            buttons: [{
              'text': 'Aceptar',
              'type': 'button-assertive',
              'onTap': function (e) {
                $state.go('app.cotizador.datos');
              }
            }]
          });

        } else {

          $scope.saveContract();

        }

      }, function (err) {

        $scope.saveContract();

      });
  };

  $scope.enviarContrato = function (contract, formData) {
    Contracts.send(formData).then(function (data) {

      //Contracts.delete(indice);
      $scope.contracts = Contracts.getAll();
      var alertPopup = $ionicPopup.alert({
        title: 'Correcto!',
        template: 'Contrato enviado exitosamente',
        okText: 'Aceptar',
        okType: 'button-balanced'
      });

      alertPopup.then(function (res) {
        $state.go('app.guest.home', {}, { reload: true });
      });

    }, function (err) {

      var email_existe = err.data && err.data.response && err.data.response.errors && err.data.response.errors['client.email'] ? true : false;

      if (email_existe) {

        var alertPopup = $ionicPopup.alert({
          title: 'Oops!',
          template: 'El email del contrato ya ha sido previamente registrado',
          buttons: [{
            'text': 'Aceptar',
            'type': 'button-assertive',
            'onTap': function (e) {

              $scope.$parent.contractData = contract;
              $scope.$parent.contractFotos = contract.fotos;
              $state.go('app.cotizador.datos');

            }
          }]
        });

      } else {

        var alertPopup = $ionicPopup.alert({
          title: 'Oops!',
          template: 'Error al enviar el contrato',
          okText: 'Aceptar',
          okType: 'button-assertive'
        });


        var error = err.data ? err.data : err;

        var obj = {
          'payload': formData,
          'error': error
        };
        $scope.currentUser.notifyError(JSON.stringify(obj));

      }

    });
  };

  $scope.enviar = function (contract) {

    var confirmPopup = $ionicPopup.confirm({
      title: 'Desea continuar?',
      template: 'Seguro que desea enviar el contrato?',
      cancelText: 'Cancelar',
      okText: 'Aceptar'
    });


    confirmPopup.then(function (res) {
      if (res) {

        $rootScope.$broadcast('loading:show');

        var pictures = {
          6: "proof_of_address",
          7: "id",
          8: "id_reverse",
          9: "profile"
        };

        var formData = new FormData();

        formData.append('client[name]', contract.name);
        formData.append('client[address]', contract.address);
        formData.append('client[phone]', contract.phone);
        formData.append('client[email]', contract.email);
        formData.append('client[between_streets]', contract.between_streets);
        formData.append('client[colony]', contract.colony);
        formData.append('client[state]', contract.state);
        formData.append('client[county]', contract.county);
        formData.append('client[postal_code]', contract.postal_code);
        formData.append('client[id_groups]', $scope.vendedor.id_groups);
        formData.append('responsabilidad', contract.responsabilidad);
        formData.append('seller', contract.seller);

        formData.append('monthly_installments', contract.monthly_installments);
        formData.append('elements[]', contract.material.id_categories_elements);

        if (contract.cupon) {
          formData.append('cupon', contract.cupon);
        }

        angular.forEach(contract.elements, function (value, key) {
          formData.append('elements[]', key);
        });

        for (var i = 0; i < contract.fotos.length; i++) {

          var blob = Imagen.base64ToBlob(contract.fotos[i]);
          if (blob) {
            var imageName = 'image' + i + '.' + (blob.mime.replace('image/', ''));
            var name = i < 6 ? 'photos[]' : pictures[i];
            formData.append(name, blob.file, imageName);
          }

        }

        if (contract.credit_card && contract.credit_card.card_number) {
          var datos = $scope.datosConekta(contract.credit_card, contract.name);
          Conekta.token.create({ "card": datos },
            function (token) {
              formData.append('credit_card[conekta]', token.id);
              if ($scope.validarTarjetaOpenPay(contract.credit_card)) {
                $scope.getTokenOpenPay(contract, formData);
              } else {
                $scope.enviarContrato(contract, formData);
              }

            },
            function (response) {
              $scope.popUp('Error al guardar la tarjeta en conekta, verifica que los datos de la tarjeta sean los correctos', false, null);
              $scope.currentUser.notifyError(JSON.stringify(response));
            }
          );
        } else {
          $scope.enviarContrato(contract, formData);
        }

      }
    });

  };

  $scope.validarTarjetaOpenPay = function (data) {
    var year = data.expiration_year.substr(data.expiration_year.length - 2);
    return (OpenPay.card.validateCardNumber(data.card_number) &&
      OpenPay.card.validateCVC(data.cvc) &&
      OpenPay.card.validateExpiry(data.expiration_month, data.expiration_year) &&
      OpenPay.card.cardType(data.card_number.toString()));
  };

  $scope.getTokenOpenPay = function (contract, formData) {
    var datos = $scope.datosOpenPay(contract);
    var deviceSessionId = OpenPay.deviceData.setup();
    OpenPay.token.create(datos, function (cardResponse) {
      formData.append('credit_card[openpay]', cardResponse.data.id);
      formData.append('credit_card[openpay_device_session_id]', deviceSessionId);
      $scope.enviarContrato(contract, formData);
    }, function (error) {
      $scope.enviarContrato(contract, formData);
    });
  };

  $scope.datosOpenPay = function (contract) {
    var year = contract.credit_card.expiration_year.substr(contract.credit_card.expiration_year.length - 2);
    return {
      "card_number": contract.credit_card.card_number,
      "holder_name": contract.name,
      "expiration_year": year,
      "expiration_month": contract.credit_card.expiration_month,
      "cvv2": contract.credit_card.cvc,
      "address": {
        "city": contract.state,
        "postal_code": contract.postal_code,
        "line1": contract.address,
        "line3": contract.between_streets,
        "state": contract.state,
        "country_code": "MX"
      }

    };
  };

  $scope.$watch('contractData', function (newVal, OldVal) {

    //console.log(newVal);
    var sum = 0;
    sum += newVal.mensual;
    angular.forEach(newVal.elements, function (value, key) {
      sum += parseFloat(value.price);
    });
    sum += Object.keys(newVal.material).length > 0 ? parseFloat(newVal.material.price) : 0;

    var resp = (sum / 1.16) * 0.007;

    if ($scope.vendedor.sign_into) {
      sum += parseFloat($scope.vendedor.sign_into.deposit) / 100;
    }
    sum += newVal.responsabilidad ? resp : 0;

    if ($scope.isValidCupon) {
      sum = sum - $scope.descuento;
    }

    if (sum < 0) {
      sum = 0;
    }

    $scope.totalCurrentContract = sum;

  }, true);

}

CotizadorMainCtrl.$inject = [
  '$scope',
  '$rootScope',
  '$state', '$filter',
  'Contracts',
  'Coupon',
  '$ionicPopup',
  '$cordovaCamera',
  '$cordovaActionSheet',
  'AuthService',
  'Imagen'
];
