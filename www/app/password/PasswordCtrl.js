function PasswordCtrl($scope,$ionicPopup,$state,SessionService) {

	//$scope.validacion = false;
	$scope.data = {};

	$scope.updatePassword = function(valido){
		if(!valido) return;

		if($scope.data.password == $scope.data.confirmacion){
			SessionService
				.changePassword($scope.data.password)
				.then(function(response){
					var alertPopup = $ionicPopup.alert({
				       title: 'Correcto!',
				       template: 'Password cambiado exitosamente',
				       okText: 'Aceptar',
				       okType: 'button-balanced'
				    });

				    SessionService.setRemember(false);

				    alertPopup.then(function(res){
				    	$state.go('app.client.welcome', {}, {reload:true});
				    });

				},function(err){
					var alertPopup = $ionicPopup.alert({
				       title: 'Oops!',
				       template: err,
				       okText: 'Aceptar',
				       okType: 'button-assertive'
				    });
				});
		}
		else
			var alertPopup = $ionicPopup.alert({
		       title: 'Oops!',
		       template: 'No coinciden la nueva contraseña y su confirmación',
		       okText: 'Aceptar',
		       okType: 'button-assertive'
		    });

	};
}

PasswordCtrl.$inject = [
    '$scope','$ionicPopup','$state','SessionService'
];