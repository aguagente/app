function MainController($scope, $rootScope, $state, $stateParams, $ionicSideMenuDelegate, SessionService, Contracts, AuthService, $ionicHistory, $ionicPopup, $cordovaFacebook) {

  $scope.currentUser = SessionService;
  $scope.menuType = "guest";
  $scope.notificaciones = 0;

  $rootScope.setCurrentUser = function (user) {
    $scope.currentUser = user;
  };

  $rootScope.getCurrentUser = function () {
    return $scope.currentUser;
  };

  $rootScope.setNotificaciones = function (x) {
    $scope.notificaciones = x;
  };

  $rootScope.getNotificaciones = function () {
    return $scope.notificaciones;
  };

  $rootScope.setMenuType = function (x) {
    $scope.menuType = x
  };

  $scope.getManyContracts = function () {
    return Contracts.getCountAll();
  };

  $scope.getManyNotificaciones = function () {
    if ($scope.menuType === "client") return 0;
    return Contracts.getCountAll();
  };

  $scope.refreshApp = function () {
    SessionService.update().then(function (data) {
      $scope.$broadcast('scroll.refreshComplete');
      var route = $state.current.name;

      if (AuthService.isAuthenticated()) {
        if (SessionService.getClientStatus() == 'invalid') {
          $state.go('app.client.editar-contrato');
        }
      }

      if (route.indexOf('seller') > -1) {
        $state.go('app.seller.welcome', {}, {reload: true});
      } else if (route.indexOf('client') > -1 || route.indexOf('login') > -1) {
        $state.go('app.guest.home', {}, {reload: true});
      } else {
        if (AuthService.isAuthenticated()) {

          var user = SessionService.getUser();

          AuthService.getUser(user.id).then(function (response) {
            var userResponse = response.data.response;

            AuthService.getClientById(user.id).then(function (data) {
              var client = data.response[0];
              userResponse.client = client;
              SessionService.setUser(userResponse);
            });

          },function (error) {

          });

          if (SessionService.getClientStatus() == 'invalid') {
            $state.go('app.client.editar-contrato');
          } else {
            $state.go($state.current, {}, {reload: true});
          }
        } else {
          $state.go($state.current, {}, {reload: true});
        }
      }


    }, function (err) {
      $scope.$broadcast('scroll.refreshComplete');
      var route = $state.current.name;
      if (route.indexOf('seller') > -1) $state.go('app.seller.welcome', {}, {reload: true});
      else if (route.indexOf('client') > -1 || route.indexOf('login') > -1) $state.go('app.guest.home', {}, {reload: true});
      else $state.go($state.current, {}, {reload: true});
    });
  };

  /*
  $scope.sendMessage = function(){
    $ionicPopup.alert({
      title: 'Reporte enviado',
      template: "Su reporte ha sido enviado, en breve recibirá nuestra respuesta.<br> Gracias.",
      buttons: [{
        'text' : 'Aceptar',
        'type' : 'button-balanced',
        'onTap' : function(e) {
          $ionicHistory.currentView($ionicHistory.backView());
          $state.go('app.guest.home', {}, {location:'replace'});
        }
      }]
    });
  };
  */

  $scope.refreshApp();

  $scope.logout = function () {

    var confirmPopup = $ionicPopup.confirm({
      title: 'Precaución!',
      template: '¿Seguro que quieres cerrar sesión?',
      cancelText: 'Cancelar',
      okText: 'Aceptar',
      okType: 'button-energized'
    });

    confirmPopup.then(function (res) {
      if (res) {

        AuthService
          .logout()
          .then(function (response) {
            /*$cordovaFacebook.logout()
                .then(function(success) {
                  // success
                }, function (error) {
                  // error
                });*/
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
            $rootScope.popupVideoShow = false;
            window.localStorage['playvideoonopen'] = true;
            AuthService.getToken();
            $state.go('app.guest.login', {}, {reload: true});

          }, function (err) {

            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
            $rootScope.popupVideoShow = false;
            window.localStorage['playvideoonopen'] = true;
            AuthService.getToken();
            $state.go('app.guest.login', {}, {reload: true});

          });
      }
    });
  };
}

MainController.$inject = [
  '$scope', '$rootScope', '$state', '$stateParams', '$ionicSideMenuDelegate', 'SessionService', 'Contracts', 'AuthService', '$ionicHistory', '$ionicPopup', '$cordovaFacebook'
];
