function ChargesCtrl(
    $scope, Charge, $ionicModal) {

	$scope.textLoad = 'Cargando cargos...';
    $scope.charges = [];
    $scope.charge = null;
	
	Charge.get({
        'id_clients': $scope.currentUser.getIdClient(),
        'conekta' : true
    }).then(function(response){
        //console.log(response);
        $scope.charges = response;
        $scope.textLoad = 'No se encontraron cargos';
    }, function(err){
        
        $scope.textLoad = 'No se encontraron cargos';
    });


    $ionicModal.fromTemplateUrl('app/modals/charge.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.chargeModal = modal;
    });

    $scope.openCharge = function(charge) {
        $scope.charge = charge;
        $scope.chargeModal.show();
    };

    $scope.closeCharge = function() {
        $scope.chargeModal.hide();
        $scope.charge = null;
    };

    $scope.$on('$destroy', function() {
        $scope.chargeModal.remove();
    });

}

ChargesCtrl.$inject = [
    "$scope", "Charge", "$ionicModal"
];