function CorreccionCtrl($scope,$ionicPopup,$rootScope,API,Imagen,Contracts,SessionService,$cordovaCamera,$cordovaActionSheet) {

	$scope.invalidation = {};
	$scope.data         = {};

	$scope.fotos = {};

	Contracts
        .getByClient( SessionService.getIdClient() )
        .then(function (response) {
        	Contracts
        	    .getInvalidations( SessionService.getIdClient() )
        	    .then(function(data){

        		response[0].invalidations = data;
        		var contrato = response[0];

        		$scope.invalidation = angular.isDefined(contrato.invalidations) ? contrato.invalidations[contrato.invalidations.length - 1] : {};
        		
        		$scope.data = {
					"id_contracts"            : contrato.id_contracts,
					"client" : {
						"phone"           : contrato.client.phone,
						"address"         : contrato.client.address,
						"between_streets" : contrato.client.between_streets,
						"colony"          : contrato.client.colony,
						"state"           : contrato.client.state,
						"county"          : contrato.client.county,
						"postal_code"     : parseInt(contrato.client.postal_code)
					}
				};

				$scope.fotos = {};

				angular.forEach(contrato.documents, function(value, key){
					if( value.search("proof_of_address") !== -1 )
						Imagen.urlToBase64(value).then(function(base64){
							$scope.fotos['proof_of_address'] = base64;
						});
					else if( value.search("profile") !== -1 )
						Imagen.urlToBase64(value).then(function(base64){
							$scope.fotos['profile'] = base64;
						});
					else if( value.search("id_reverse") !== -1 )
						Imagen.urlToBase64(value).then(function(base64){
							$scope.fotos['id_reverse'] = base64;
						});
					else if( value.search("id") !== -1 )
						Imagen.urlToBase64(value).then(function(base64){
							$scope.fotos['id'] = base64;
						});
				});

				angular.forEach(contrato.photos, function(value, key){
					if( value.search("image0") !== -1 ){
						Imagen.urlToBase64(value).then(function(base64){
							$scope.fotos['image0'] = base64;
						});
					}
					else if( value.search("image1") !== -1 ){
						Imagen.urlToBase64(value).then(function(base64){
							$scope.fotos['image1'] = base64;
						});
					}
					else if( value.search("image2") !== -1 )
						Imagen.urlToBase64(value).then(function(base64){
							$scope.fotos['image2'] = base64;
						});
					else if( value.search("image3") !== -1 )
						Imagen.urlToBase64(value).then(function(base64){
							$scope.fotos['image3'] = base64;
						});
					else if( value.search("image4") !== -1 )
						Imagen.urlToBase64(value).then(function(base64){
							$scope.fotos['image4'] = base64;
						});
					else if( value.search("image5") !== -1 )
						Imagen.urlToBase64(value).then(function(base64){
							$scope.fotos['image5'] = base64;
						});
				});

        	});

        }, function(err){ 

        	var alertPopup = $ionicPopup.alert({
		       title: 'Oops!',
		       template: angular.isDefined(err.data.message) ? err.data.message : 'Error al obtener el contrato',
		       okText: 'Aceptar',
		       okType: 'button-assertive'
		    });
		    
        });


    $scope.removeImage = function(index){
		$scope.fotos[index] = null;
		$scope.data[index]  = null;
    };

    $scope.cameraImage = function(index, type){
        
        var options = {
            quality: 40,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: type,
            allowEdit: false,
            encodingType: Camera.EncodingType.JPEG,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation:true
        };

        $cordovaCamera
            .getPicture(options)
            .then(function(imageData) {
            	$scope.fotos[index] = 'data:image/jpeg;base64,'+imageData;
            }, function(err) {
                console.log(err);
            });

    };

    $scope.captureImage = function(index){
        
        var options = {
            title: 'Metodo para obtener la imagen',
            buttonLabels: ['Carrete/Galeria', 'Hacer foto'],
            addCancelButtonWithLabel: 'Cancelar',
            androidEnableCancelButton : true,
            winphoneEnableCancelButton : true
        };

        var types = {
            1 : Camera.PictureSourceType.PHOTOLIBRARY,
            2 : Camera.PictureSourceType.CAMERA
        };

        $cordovaActionSheet
            .show(options)
            .then(function(btnIndex){

                if(btnIndex == 1 || btnIndex == 2)
                    $scope.cameraImage(index,types[btnIndex]);

            });
    };

    $scope.updateContract = function(valido){

    	if(!valido) return;

		var confirmPopup = $ionicPopup.confirm({
			title      : 'Desea continuar?',
			template   : 'Seguro que desea enviar el contrato?',
			cancelText : 'Cancelar',
			okText     : 'Aceptar'
		});


		confirmPopup.then(function(res){
			if(res){

				$rootScope.$broadcast('loading:show');
				
				$scope.data.upload_documents = {};
				$scope.data.upload_photos = {};
				
				for(var key in $scope.fotos){

					var blob = Imagen.base64ToBlob($scope.fotos[key]);
					if(blob){
						if(key.search("image") !== -1)
							$scope.data.upload_photos[key] = blob;
						else
							$scope.data.upload_documents[key] = blob;
					}

				}

				Contracts.toCorrect($scope.data).then(function(data){

					var alertPopup = $ionicPopup.alert({
				       title: 'Correcto!',
				       template: 'Contrato enviado exitosamente',
				       okText: 'Aceptar',
				       okType: 'button-balanced'
				    });

				    alertPopup.then(function(res) {
						$scope.refreshApp();
					});


				}, function(err){

					var errMsj = angular.isDefined( err.data.response ) && angular.isDefined( err.data.response.errors ) && angular.isDefined( err.data.response.errors['client.email'] ) ? 'Email registrado anteriormente en otro contrato' : 'Error al enviar el contrato';

					var alertPopup = $ionicPopup.alert({
				       title: 'Oops!',
				       template: errMsj,
				       okText: 'Aceptar',
				       okType: 'button-assertive'
				    });

				});
				
			}
		});

	};
	
}

CorreccionCtrl.$inject = [
    '$scope','$ionicPopup','$rootScope','API','Imagen','Contracts', 'SessionService','$cordovaCamera', '$cordovaActionSheet'
];
