function ContactosCtrl($scope,$rootScope,$timeout,$filter,$ionicPopup,API){

	$scope.friends=[];
	$scope.friendsFavorites={};
	$scope.canLoad=null;
	$scope.loading=!1;
	$scope.favorites=[];
	$scope.data={};
	$scope.all = true;

	$scope.emailsNotNull = function(p){
		return (p.emails != null);
	};

	$scope.toggleFavorite=function(friend){

		var amigo = {
			name : $filter("contactName")(friend),
			id : friend.id,
			picture : friend.photos ? friend.photos[0].value : friend.picture ? friend.picture : "",
			email : friend.emails[0].value
		};

		if($scope.friendsFavorites[amigo.id]){
			delete $scope.friendsFavorites[amigo.id];
		}
		else{
			$scope.friendsFavorites[amigo.id]=amigo;
		}

	};

	$scope.getAmigos=function(){

		$rootScope.$broadcast("loading:show");
		var options=new ContactFindOptions;
		options.filter="";
		options.multiple=!0;
		var filter=["displayName","name"];
		navigator.contacts
			.find(filter,
				function(contacts){
					$timeout(function(){
						$rootScope.$broadcast("loading:hide")
					},4e3);
					$scope.friends=contacts;
					console.log(contacts);
				},function(err){
					$rootScope.$broadcast("loading:hide");
					$ionicPopup.alert({
                        title: 'Error!',
                        template: 'No se pudo obtener contactos del telefono',
                        buttons: [{
                            'text' : 'Aceptar',
                            'type' : 'button-assertive'
                        }]
                    });
				},
			options);
	};

	$scope.getAmigos();

	$scope.filterAmigos=function(){
		if(""==$scope.data.search||!$scope.data.search)
			return; 
		//$scope.getAmigos();
		$rootScope.$broadcast("loading:show");
		var options=new ContactFindOptions;
		options.filter=angular.copy($scope.data.search);
		options.multiple=!0;
		var filter=["displayName","name"];
		navigator.contacts
			.find(filter,
				function(contacts){
					$timeout(function(){
						$rootScope.$broadcast("loading:hide");
					},1500);
					$scope.friends=contacts;
					console.log(contacts);
				},function(err){
					$rootScope.$broadcast("loading:hide");
					$ionicPopup.alert({
                        title: 'Error!',
                        template: 'No se pudo obtener contactos del telefono',
                        buttons: [{
                            'text' : 'Aceptar',
                            'type' : 'button-assertive'
                        }]
                    });
				},
			options);
	};

	$scope.enviarEmail = function(){

		var user = $scope.currentUser.getUser();

		var para = [];
		angular.forEach( $scope.friendsFavorites, function(value,key){
			this.push(value.email);
		}, para);

		var properties = {
			to: user.email,
			cc: '',
			bcc: para,
			attachments: [],
			subject:'Te invito a unirte al mundo de Aguagente',
			body: '<div style="text-align:center;"><img src="http://www.aguagente.dreamhosters.com/wp-content/uploads/2015/11/logocentrado250.png" style="width:50%; margin:0px auto;" /> <br/><br/> Hola, te invito al mundo de aguagente, ven y conoce mas sobre nosotros. <br/> <a href="'+API.FACE+'/'+$scope.currentUser.getIdClient()+'"><h2>Unirme al servicio de aguagente</h2></a></div>',
			isHtml: true
		};

		//var body = '<div style="text-align:center;"><img src="http://www.aguagente.dreamhosters.com/wp-content/uploads/2015/11/logocentrado250.png" style="width:50%; margin:0px auto;" /> <br/><br/> Hola, te invito al mundo de aguagente, ven y conoce mas sobre nosotros. <br/> <a href="'+API.FACE+'/'+$scope.currentUser.getIdClient()+'"><h2>Unirme al servicio de aguagente</h2></a></div>';

		if($scope.medios.length > 0){

			var  item = $scope.medios[0];
			var es_url = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/.test(item.text);
	        var img = (/\.(gif|jpg|jpeg|tiff|png)$/i).test(item.image);
	        var link = es_url ? item.text : (img ? item.image : '');
	        var title = item.title != '' ? 'Ver multimedia '+item.title : 'Ver multimedia';
			properties.body = '<div style="text-align:center;"><img src="http://www.aguagente.dreamhosters.com/wp-content/uploads/2015/11/logocentrado250.png" style="width:50%; margin:0px auto;" /> <br/><br/> Hola, te invito al mundo de aguagente, ven y conoce mas sobre nosotros. <br/> <a href="'+API.FACE+'/'+$scope.currentUser.getIdClient()+'"><h2>Unirme al servicio de aguagente</h2></a><br/><a href="'+link+'">'+title+'</a></div>';

		}

		cordova.plugins.email.open(properties, function () {
		    console.log('email view dismissed');
		}, this);

	};

	$scope.selectAll = function(){

		angular.forEach($scope.friends, function(friend,key){
			
			if(friend.emails){

				var amigo = {
					name : $filter("contactName")(friend),
					id : friend.id,
					picture : friend.photos ? friend.photos[0].value : friend.picture ? friend.picture : "",
					email : friend.emails[0].value
				};

				if($scope.all && !$scope.friendsFavorites[amigo.id]){
					$scope.friendsFavorites[amigo.id]=amigo;
				}
				else if(!$scope.all && $scope.friendsFavorites[amigo.id]){
					delete $scope.friendsFavorites[amigo.id];
				}

			}

		});

		$scope.all = !$scope.all;

	};

}

ContactosCtrl.$inject=[
	"$scope","$rootScope","$timeout","$filter","$ionicPopup","API"
];