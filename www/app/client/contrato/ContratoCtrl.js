function ClientContratoCtrl($scope, SessionService, Contracts, Coupon, Payment, $filter) {

  $scope.data = {};
  $scope.fees = {};
  $scope.bank = null;
  $scope.total_extras = 0;
  $scope.resp_social = 0;
  $scope.resp_mensual = 0;
  $scope.cupon = null;
  $scope.descuento = 0;

  Contracts
    .getByClient(SessionService.getIdClient())
    .then(function (response) {

      $scope.data = response.length ? response[0] : {};

      angular.forEach(response[0].elements, function (value, key) {
        $scope.total_extras += parseFloat(value.price);
      });

      if ($scope.data.elements.length > 0) {
        var acero = $filter('filter')($scope.data.elements, { 'element_name': 'Acero inoxidable' })[0];
        console.log(acero);
        if (acero) {
          acero.price = parseFloat($scope.data.client.group.sign_into.installation_fee) / 100;
        }
      }

      if (response[0].client.social_responsability == 1) {
        $scope.resp_social = ($scope.total_extras / 1.16) * 0.007;
        $scope.resp_mensual = ((response[0].client.monthly_fee / 100) / 1.16) * 0.007;
      }

      if (response[0].client.coupon != 0 && response[0].client.coupon) {
        $scope.cupon = response[0].client.coupon;
        $scope.descuento = Coupon.getDiscount($scope.cupon.level);
      }

      $scope.total_extras += $scope.resp_social;

    });

  Payment.getAll().then(function (response) {

    $scope.cards = response;
    for (var i in response) {
      $scope.bank = response[i].brand;
      if (response[i].brand == "AMERICAN_EXPRESS") {
        break;
      }
    }

  });

  Payment.getFees().then(function (response) {
    $scope.fees = response;
  });
}

ClientContratoCtrl.$inject = [
  '$scope', 'SessionService', 'Contracts', 'Coupon', 'Payment', '$filter'
];
