function ClientMainCtrl($scope, $rootScope, $ionicPopup, API, MediaGallery, $cordovaDialogs, $cordovaSocialSharing, $ionicModal) {

  $rootScope.setMenuType("guest");
  $scope.medios = [];
  $scope.textLoad = 'Cargando medios...';
  $scope.media = null;

  MediaGallery.get().then(function (response) {
    $scope.medios = response;
    $scope.textLoad = 'No se encontraron medios';
  }, function (err) {
    $scope.textLoad = 'No se encontraron medios';
  });

  $ionicModal.fromTemplateUrl('app/modals/postMedia.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.postModal = modal;
  });

  $scope.openMedia = function (media) {
    $scope.media = media;
    $scope.postModal.show();
  };

  $scope.closeMedia = function () {
    $scope.postModal.hide();
    $scope.media = null;
  };

  $scope.$on('$destroy', function () {
    $scope.postModal.remove();
  });

  $scope.referirWhatsApp = function () {
    if ($scope.medios.length > 0) {
      $scope.shareWhatsApp($scope.medios[0]);
    } else
      $cordovaSocialSharing
        .shareViaWhatsApp('Unete al mundo de Aguagente!', null, API.FACE + '/' + $scope.currentUser.getIdClient())
        .then(function (result) {
          // Success!
        }, function (err) {

          $ionicPopup.alert({
            title: 'Oops!',
            template: 'Ocurrio un error al compartir por whatsapp',
            buttons: [{
              'text': 'Aceptar',
              'type': 'button-assertive'
            }]
          });
        });
  };

  $scope.shareWhatsApp = function (item) {
    var es_url = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/.test(item.text);
    var img = (/\.(gif|jpg|jpeg|tiff|png)$/i).test(item.image);
    var link = es_url ? item.text : item.image;

    $cordovaSocialSharing
      .shareViaWhatsApp(item.title + ' ' + (es_url ? link : item.text) + ' Registrate en: ' + (img ? API.FACE + '/' + $scope.currentUser.getIdClient() : ''), (img ? item.image : null), (img ? null : API.FACE + '/' + $scope.currentUser.getIdClient()))
      .then(function (result) {
        // Success!
      }, function (err) {

        $ionicPopup.alert({
          title: 'Oops!',
          template: 'Ocurrio un error al compartir por whatsapp',
          buttons: [{
            'text': 'Aceptar',
            'type': 'button-assertive'
          }]
        });
      });
  };

  $scope.shareIt = function () {
    var media = angular.copy($scope.media);
    var es_url = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/.test(media.text);
    var img = (/\.(gif|jpg|jpeg|tiff|png)$/i).test(media.image);
    var link = es_url ? media.text : media.image;

    $scope.closeMedia();

    var opc = {
      method: "feed",
      //picture: media.image,
      name: 'Aguagente',
      message: media.text,
      href: (img ? media.image : link),
      caption: media.title,
      description: media.text
    };

    facebookConnectPlugin.showDialog(opc, function (res) {
      $scope.referirFacebook();
    }, function () {
      $ionicPopup.alert({
        title: 'Oops!',
        template: 'Ocurrio un error al compartir por facebook',
        buttons: [{
          'text': 'Aceptar',
          'type': 'button-assertive'
        }]
      });
    });
  };

  $scope.referirFace = function () {
    if ($scope.medios.length > 0) {
      $scope.openMedia($scope.medios[0]);
    } else
      $scope.referirFacebook();

  };

  $scope.referirFacebook = function () {
    facebookConnectPlugin.showDialog({
      method: "share",
      href: API.FACE + '/' + $scope.currentUser.getIdClient(),
      caption: 'Regístrate en Aguagente',
      description: 'Entra, conoce y disfruta el mundo de aguagente',
      picture: 'http://panel.aguagente.com/registrarme.png'
    }, function () {
      console.log('finalizo comparticion por face');
      $cordovaGoogleAnalytics.trackEvent('Media','click','Click Medio', 1);
      $ionicPopup.alert({
        title: 'Correcto!',
        template: 'Gracias por compartir en facebook',
        buttons: [{
          'text': 'Aceptar',
          'type': 'button-balanced'
        }]
      });

    }, function () {

      $ionicPopup.alert({
        title: 'Oops!',
        template: 'Ocurrio un error al compartir por facebook',
        buttons: [{
          'text': 'Aceptar',
          'type': 'button-assertive'
        }]
      });
    });
  };
}

ClientMainCtrl.$inject = [
  '$scope', '$rootScope', '$ionicPopup', 'API', 'MediaGallery', '$cordovaDialogs', '$cordovaSocialSharing', '$ionicModal'
];
