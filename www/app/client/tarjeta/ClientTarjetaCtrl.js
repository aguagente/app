function ClientTarjetaCtrl($scope, $state, Payment, SessionService, $ionicPopup, $ionicPopover) {
	$scope.amex = false;
	$scope.cardObject = {};
	$scope.data = {};

	Payment
		.getAll()
		.then(function (response) {
			for (var i in response) {
				if (response[i].brand == "AMERICAN_EXPRESS") {
					$scope.amex = true;
					break;
				}
			}
		});


	$scope.openPopover = function ($event, templateName) {
		// Init popover on load
		$ionicPopover.fromTemplateUrl(templateName, {
			scope: $scope,
		}).then(function (popover) {
			$scope.popover = popover;
			$scope.popover.show($event);
		});
	};

	$scope.closePopover = function () {
		$scope.popover.hide();
	};

	$scope.$on('$destroy', function () {
		if ($scope.popover){
		  $scope.popover.remove();
    }
	});

	$scope.setMonth = function(month) {
		$scope.data.expiration_month = month;
		$scope.closePopover();
	}


	$scope.setYear = function (year) {
		$scope.data.expiration_year = year;
		$scope.closePopover();
	}

	$scope.setBrand = function (brand) {
		switch (brand) {
			case 'visa':
				$scope.brand_name = ' VISA';
				break;
			case 'mastercard':
				$scope.brand_name = ' MASTERCARD';
				break;
			case 'amex':
				$scope.brand_name = ' AMEX';
				break;
		}
		$scope.data.bank = brand;
		$scope.closePopover();
	}



	$scope.popUp = function (msj, success, callback) {
		var alertPopup = $ionicPopup.alert({
			title: success ? 'Correcto!' : 'Ooops!',
			template: msj,
			buttons: [{
				'text': 'Aceptar',
				'type': success ? 'button-balanced' : 'button-assertive',
				'onTap': callback
			}]
		});
	};
	$scope.datosConekta = function (data) {
		return {
			"name": SessionService.getUser().client.name,
			"number": data.card_number,
			"cvc": data.cvc,
			"exp_month": data.expiration_month,
			"exp_year": data.expiration_year
		};
	};

	$scope.validarTarjeta = function (data) {
		if (!Conekta.card.validateNumber(data.card_number)) return 'Número de tarjeta invalido';
		if (!Conekta.card.validateCVC(data.cvc)) return 'CVC incorrecto';
		if (!Conekta.card.validateExpirationDate(data.expiration_month, data.expiration_year)) return 'Fecha de vencimiento invalida';
		if (!data.bank) return 'Tipo de tarjeta obligatorio';
		if (Conekta.card.getBrand(data.card_number.toString()) != data.bank) return 'Tipo de tarjeta incorrecto';
		return false;
	};

	$scope.validarTarjetaOpenPay = function (data) {
		var year = data.expiration_year.substr(data.expiration_year.length - 2);
		return (OpenPay.card.validateCardNumber(data.card_number) &&
			OpenPay.card.validateCVC(data.cvc) &&
			OpenPay.card.validateExpiry(data.expiration_month, data.expiration_year) &&
			OpenPay.card.cardType(data.card_number.toString()));
	};

	$scope.enviar = function () {

		var error = $scope.validarTarjeta($scope.data);
		if (error) {
			$scope.popUp(error, false, null);
		} else {
			var datos = $scope.datosConekta($scope.data);
			Conekta.token.create({ "card": datos },
				function (token) {
					$scope.cardObject.conekta = token.id;
					if ($scope.validarTarjetaOpenPay($scope.data)) {
						$scope.getTokenOpenPay();
					} else {
						$scope.saveCard($scope.cardObject);
					}
				},
				function (response) {
					$scope.popUp('Error al guardar la tarjeta', false, null);
				}
			);
		}
		return;
	};

	$scope.getTokenOpenPay = function () {
		var deviceSessionId = OpenPay.deviceData.setup();
		var datos = $scope.datosOpenPay($scope.data);
		OpenPay.token.create(datos, function (cardResponse) {
			$scope.cardObject.openpay = cardResponse.data.id;
			$scope.saveCard($scope.cardObject, deviceSessionId);
		}, function (error) {
			$scope.saveCard($scope.cardObject);
		});
	};

	$scope.datosOpenPay = function (data) {
		var user = SessionService.getUser();
		var year = data.expiration_year.substr(data.expiration_year.length - 2);
		return {
			"card_number": data.card_number,
			"holder_name": user.client.name,
			"expiration_year": year,
			"expiration_month": data.expiration_month,
			"cvv2": data.cvc,
			"address": {
				"city": user.client.state,
				"postal_code": user.client.postal_code,
				"line1": user.client.address,
				"line2": user.client.outdoor_number,
				"line3": user.client.between_streets,
				"state": user.client.state,
				"country_code": "MX"
			}

		};
	};

	$scope.saveCard = function (card, deviceSessionId = null) {
		Payment.create(card, deviceSessionId).then(function (response) {
			$state.transitionTo("app.client.tarjetas", null, {
				reload: true,
				inherit: true,
				notify: true
			});
		}, function (response) {
			$scope.popUp('Error al guardar la tarjeta', false, null);
		});
	}

}

ClientTarjetaCtrl.$inject = [
	'$scope', '$state', 'Payment', 'SessionService', '$ionicPopup', '$ionicPopover'
];
