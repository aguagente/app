function ClientTarjetaListCtrl($scope, $state, $filter, Payment, Contracts, SessionService, $ionicPopup) {

    var user = $scope.currentUser.getUser();

	$scope.cards = [];
    $scope.amex = false;
    $scope.data = user.client;

    Payment.getAll().then(function(response){
        $scope.cards = response;

        for(var i in response){
            if(response[i].brand == "AMERICAN_EXPRESS"){
                $scope.amex = true;
                break;
            }
        }

    });

    $scope.popUp = function(msj,success,callback){

		var alertPopup = $ionicPopup.alert({
			title: success ? 'Correcto!'  : 'Ooops!',
			template: msj,
			buttons: [{
				'text' : 'Aceptar',
				'type' : success ? 'button-balanced' : 'button-assertive',
				'onTap' : callback
			}]
		});
		
	};


    $scope.principal = function(card){

        var confirmPopup = $ionicPopup.confirm({
            title: 'Tarjeta Principal',
            template: 'Desea utilizar esta tarjeta como principal?',
            cancelText: 'Cancelar',
            okText: 'Aceptar'
        });

        confirmPopup.then(function(res){
            if(res){
                Payment.setAsDefault(card.id).then(function(response){
                    var defaultBefore = $filter('filter')($scope.cards,{'default':true})[0];
                    defaultBefore.default = false;
                    card.default = true;
                    $scope.popUp('Tarjeta principal guardada correctamente',true,null);
                },function(err){
                    $scope.popUp('Error al guardar la tarjeta principal',false,null);
                });
            }
        });
            	
    };

    $scope.eliminar = function(card,i){

        if(card.default){
            $scope.popUp('Error la tarjeta principal no puede ser eliminada',false,null);
            return;
        }

        var confirmPopup = $ionicPopup.confirm({
            title: 'Eliminar tarjeta',
            template: 'Seguro que desea eliminar esta tarjeta?',
            cancelText: 'Cancelar',
            okText: 'Aceptar',
            okType: 'button-assertive'
        });

        confirmPopup.then(function(res){
            if(res){
                Payment.delete(card.id).then(function(data){
                    $scope.cards.splice(i,1);
                    $scope.popUp('Tarjeta eliminada correctamente',true,null);
                },function(err){
                    $scope.popUp('Error al eliminar la tarjeta',false,null);
                });
            }
        });
    };

    $scope.enviarFormaPago = function(){

        Contracts
            .toCorrect(
                {
                    "client" : $scope.data,
                    "id_contracts" : $scope.data.contract.id_contracts
                }
            )
            .then(function(response){

                SessionService.update().then(function(data){
                    $scope.popUp('Forma de pago actualizada correctamente',true,null);
                });

            }, function(err){
                $scope.popUp('Error al actualizar la forma de pago',false,null);
            });

    };

}

ClientTarjetaListCtrl.$inject = [
    '$scope', '$state', '$filter' , 'Payment', 'Contracts','SessionService','$ionicPopup'
];