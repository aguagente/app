angular.module('aguagenteclient')
    .config(function($stateProvider) {
        $stateProvider

            .state('app.client', {
                cache: false,
                url: '/client',
                abstract: true,
                views: {
                    'menuContent@app': {
                        template: '<ion-view><ion-nav-view name="clientContent"></ion-nav-view></ion-view>',
                        controller: 'ClientMainCtrl'
                    }
                }
            })
            /*.state('app.client.welcome', {
              cache: false,
              url: '/welcome',
              views: {
                'clientContent': {
                  templateUrl: 'app/client/welcome/welcome.html',
                }
              }
            })*/
            .state('app.client.password', {
                cache: false,
                url: '/password',
                views: {
                    'clientContent': {
                        templateUrl: 'app/password/password.html',
                        controller: 'PasswordCtrl'
                    }
                }
            })
            .state('app.client.notificaciones', {
                cache: false,
                url: '/notificaciones',
                views: {
                    'clientContent': {
                        templateUrl: 'app/notificaciones/notificaciones.html',
                        controller: 'NotificacionesCtrl'
                    }
                }
            })
            .state('app.client.contactos', {
                cache: false,
                url: '/contactos',
                views: {
                    'clientContent': {
                        templateUrl: 'app/client/contactos/contactos.html',
                        controller: 'ContactosCtrl'
                    }
                }
            })
            .state('app.client.contrato', {
                cache: false,
                url: '/contrato',
                views: {
                    'clientContent': {
                        templateUrl: 'app/client/contrato/contrato.html',
                        controller: 'ClientContratoCtrl'
                    }
                }
            })
            .state('app.client.editar-contrato', {
                cache: false,
                url: '/editar-contrato',
                views: {
                    'clientContent': {
                        templateUrl: 'app/client/editar-contrato/editar-contrato.html',
                        controller: 'ClientEditarContratoCtrl'
                    }
                }
            })
            .state('app.client.correccion', {
                cache: false,
                url: '/correccion',
                views: {
                    'clientContent': {
                        templateUrl: 'app/client/correccion/correccion.html',
                        controller: 'CorreccionCtrl'
                    }
                }
            })
            .state('app.client.tarjetas', {
                cache: false,
                url: '/tarjetas',
                views: {
                    'clientContent': {
                        templateUrl: 'app/client/tarjeta/tarjetas.html',
                        controller: 'ClientTarjetaListCtrl'
                    }
                }
            })
            .state('app.client.tarjeta', {
                cache: false,
                url: '/tarjeta',
                views: {
                    'clientContent': {
                        templateUrl: 'app/client/tarjeta/tarjeta.html',
                        controller: 'ClientTarjetaCtrl'
                    }
                }
            })
            .state('app.client.media', {
                cache: false,
                url: '/media',
                views: {
                    'clientContent': {
                        templateUrl: 'app/client/media/media.html',
                        controller: 'MediaCtrl'
                    }
                }
            })
            .state('app.client.charges', {
                cache: false,
                url: '/charges',
                views: {
                    'clientContent': {
                        templateUrl: 'app/client/charges/charges.html',
                        controller: 'ChargesCtrl'
                    }
                }
            })
            .state('app.client.contract', {
                url: '/contract',
                abstract: true,
                cache: false,
                views: {
                    'clientContent': {
                        template: '<ion-view><ion-nav-view name="contractContent"></ion-nav-view></ion-view>',
                        controller: 'ContractMainCtrl'
                    }
                }
            })
            .state('app.client.contract.welcome', {
                cache: true,
                url: '/welcome',
                views: {
                    'contractContent': {
                        templateUrl: 'app/seller/welcome/welcome.html'
                    }
                }
            })
            .state('app.client.contract.cotizador', {
                cache: false,
                url: '/cotizado',
                views: {
                    'contractContent': {
                        templateUrl: 'app/seller/cotizador/cotizador.html',
                        controller: 'CotizadorCtrl'
                    }
                }
            })
            .state('app.client.contract.encuestas', {
                cache: false,
                url: '/encuestas',
                views: {
                    'contractContent': {
                        templateUrl: 'app/seller/encuestas/encuestas.html',
                        controller: 'EncuestaListCtrl'
                    }
                }
            })
            .state('app.client.contract.encuesta', {
                cache: false,
                url: '/encuesta/:id',
                views: {
                    'contractContent': {
                        templateUrl: 'app/seller/encuestas/encuesta.html',
                        controller: 'EncuestaCtrl'
                    }
                }
            })
            .state('app.client.contract.extras', {
                cache: false,
                url: '/extras',
                views: {
                    'contractContent': {
                        templateUrl: 'app/seller/extras/extras.html',
                        controller: 'ExtraCtrl'
                    }
                }
            })
            .state('app.client.contract.mensual', {
                cache: false,
                url: '/mensual',
                views: {
                    'contractContent': {
                        templateUrl: 'app/seller/mensual/mensual.html',
                        controller: 'MensualCtrl'
                    }
                }
            })
            .state('app.client.contract.fotos', {
                cache: false,
                url: '/fotos',
                views: {
                    'contractContent': {
                        templateUrl: 'app/seller/fotos/fotos.html'
                    }
                }
            })
            .state('app.client.contract.datos', {
                cache: false,
                url: '/datos',
                views: {
                    'contractContent': {
                        templateUrl: 'app/seller/datos/datos.html'
                    }
                }
            })
            .state('app.client.contract.tarjeta', {
                cache: false,
                url: '/tarjeta',
                views: {
                    'contractContent': {
                        templateUrl: 'app/seller/tarjeta/tarjeta.html',
                        controller: 'SellerTarjetaCtrl'
                    }
                }
            })
            .state('app.client.contract.preview', {
                cache: false,
                url: '/preview',
                views: {
                    'contractContent': {
                        templateUrl: 'app/seller/preview/preview.html',
                        controller: 'PreviewCtrl'
                    }
                }
            })
            .state('app.client.contract.contratos', {
                cache: false,
                url: '/contratos',
                views: {
                    'contractContent': {
                        templateUrl: 'app/seller/contratos/contratos.html',
                        controller: 'ContratosCtrl'
                    }
                }
            })
    });