function ClientEditarContratoCtrl($scope, SessionService, Contracts, Coupon, Payment, $cordovaCamera, $cordovaActionSheet, $filter, $ionicPopup, $rootScope, $state, Imagen, AuthService, $ionicHistory, Categories, $ionicScrollDelegate) {

  $scope.data = {};
  $scope.fees = {};
  $scope.bank = null;
  $scope.total_extras = 0;
  $scope.resp_social = 0;
  $scope.resp_mensual = 0;
  $scope.cupon = null;
  $scope.descuento = 0;
  $scope.contractFotos = [];
  $scope.categories = [];

  $scope.stepOne = true;
  $scope.stepTwo = false;
  $scope.isValidCupon = true;
  $scope.totalCurrentContract = 0;
  $scope.contractData = {
    client: {}
  };
  $scope.group = {
    sign_into: {}
  };
  $scope.vendedor = {
    sign_into: null
  };
  $scope.contractData.material = {};
  $rootScope.$broadcast('loading:show');
  $scope.elementsContract = [];
  $scope.errorMaterial = false;

  $scope.filter = {
    blue: false
  };


  angular.element(document).ready(function () {
    $scope.getContract();
  });

  $scope.getContract = function () {
    Contracts
      .getByClient(SessionService.getIdClient())
      .then(function (response) {
        $scope.data = response.length > 0 ? response[0] : {};
        $scope.contractData = response.length > 0 ? response[0] : {};
        $scope.contractData.upload_documents = {};
        $scope.contractFotos = $scope.contractData.photos;
        $scope.contractData.mensual = $scope.data.client.group.monthly_fee / 100;
        $scope.contractData.cupon = null;
        $scope.contractData.monthly_installments = 1;
        $scope.contractData.fee_installments = 0;
        $scope.contractData.material = {};
        $scope.elementsContract = angular.copy($scope.contractData.elements);
        $scope.contractData.elements = [];
        $scope.vendedor = $scope.contractData.client.group;
        $scope.group = $scope.vendedor;
        $scope.filter.blue = $scope.contractData.client.social_responsability;
        $scope.contractData.client.social_responsability = true;
        $scope.deposito = $scope.contractData.client.deposit;
        angular.forEach($scope.contractData.documents, function (value) {
          if (value.indexOf('proof_of_address') !== -1) {
            $scope.contractData.upload_documents.proof_of_address = value
          } else if (value.indexOf('profile') !== -1) {
            $scope.contractData.upload_documents.profile = value;
          } else if (value.indexOf('id.') !== -1) {
            $scope.contractData.upload_documents.id = value;
          } else if (value.indexOf('reverse.') !== -1) {
            $scope.contractData.upload_documents.id_reverse = value;
          }
        });

        if ($scope.contractData.cupon === null) {
          $scope.isValidCupon = false;
        }
        $scope.getCategories();
      });
  };


  $scope.getCategories = function () {
    Categories.getAll().then(function (response) {
      $scope.categories = response;

      $scope.material = $filter('filter')(response, {'name': 'Material'})[0];
      console.log($scope.material);
      if ($scope.material) {
        var acero = $filter('filter')($scope.material.elements, {'name': 'Acero inoxidable'})[0];
        if (acero) {
          acero.price = parseFloat($scope.vendedor.sign_into.installation_fee) / 100;
        }
      }

      if ($scope.elementsContract.length > 0) {
        var materialSelected = $filter('filter')($scope.elementsContract, {'category_name': 'Material'})[0];
        var elegido = $filter('filter')($scope.material.elements, {'name': materialSelected.element_name});
        $scope.changeMaterial(elegido[0], {target: {checked: true}});
      }else{
        var acero = $filter('filter')($scope.material.elements, {'name': 'Acero inoxidable'})[0];
        $scope.contractData.material.id_categories_elements == acero.id_categories_elements;
        $scope.changeMaterial(acero, {target: {checked: true}});
      }


      $rootScope.$broadcast('loading:hide');

      $scope.setElementsContract();
    });
  };

  $scope.setElementsContract = function () {
    var extras = $filter('filter')($scope.categories, {'category_name': '!Material'});
    angular.forEach(extras, function (category) {
      angular.forEach(category.elements, function (extra) {
        $scope.verifyExtra(extra, category);
      });
    });
  };

  $scope.changeMaterial = function (elem, $event) {
    console.log('aqui estamos')
    if ($event.target.checked) {
      $scope.errorMaterial = false;
      $scope.contractData.material = elem;
    } else {
      $scope.errorMaterial = true;
      $scope.contractData.material = {};
    }
  };

  $scope.captureImage = function (index) {

    var options = {
      title: 'Metodo para obtener la imagen',
      buttonLabels: ['Carrete/Galeria', 'Hacer foto'],
      addCancelButtonWithLabel: 'Cancelar',
      androidEnableCancelButton: true,
      winphoneEnableCancelButton: true
    };

    var types = {
      1: Camera.PictureSourceType.PHOTOLIBRARY,
      2: Camera.PictureSourceType.CAMERA
    };

    $cordovaActionSheet
      .show(options)
      .then(function (btnIndex) {

        if (btnIndex == 1 || btnIndex == 2)
          $scope.cameraImage(index, types[btnIndex]);

      });
  };

  $scope.cameraImage = function (index, type) {

    var options = {
      quality: 40,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: type,
      allowEdit: false,
      encodingType: Camera.EncodingType.JPEG,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    $cordovaCamera
      .getPicture(options)
      .then(function (imageData) {
        switch (index) {
          case 6:
            $scope.contractData.upload_documents.proof_of_address = 'data:image/jpeg;base64,' + imageData;
            break;
          case 7:
            $scope.contractData.upload_documents.profile = 'data:image/jpeg;base64,' + imageData;
            break;
          case 8:
            $scope.contractData.upload_documents.id = 'data:image/jpeg;base64,' + imageData;
            break;
          case 9:
            $scope.contractData.upload_documents.id_reverse = 'data:image/jpeg;base64,' + imageData;
            break;

          default:
            $scope.contractFotos[index] = 'data:image/jpeg;base64,' + imageData;
            break;
        }
      }, function (err) {

      });

  };

  $scope.removeImage = function (index) {
    $scope.contractFotos[index] = null;
  };

  $scope.saveContract = function () {
    $scope.contractData.fecha = $filter('date')(new Date(), 'yyyy-MM-dd');
    var contrato = angular.copy($scope.contractData);
    contrato.fotos = angular.copy($scope.contractFotos);


    $scope.enviar(contrato);
  };

  $scope.enviar = function (contract) {

    var confirmPopup = $ionicPopup.confirm({
      title: 'Desea continuar?',
      template: 'Seguro que desea enviar el contrato?',
      cancelText: 'Cancelar',
      okText: 'Aceptar'
    });

    confirmPopup.then(function (res) {
      if (res) {
        $rootScope.$broadcast('loading:show');
        angular.forEach(contract.upload_documents, function (value, key) {
          if (value && value.length > 150) {
            switch (key) {
              case 'proof_of_address':
                contract.upload_documents.proof_of_address = Imagen.base64ToBlob(value);
                break;
              case 'profile':
                contract.upload_documents.profile = Imagen.base64ToBlob(value);
                break;
              case 'id':
                contract.upload_documents.id = Imagen.base64ToBlob(value);
                break;
              case 'id_reverse':
                contract.upload_documents.id_reverse = Imagen.base64ToBlob(value);
                break;
            }
          }
        });
        $scope.enviarContrato(contract);

      }
    });

  };


  $scope.enviarContrato = function (contract) {
    delete contract.photos;
    Contracts.toCorrect(contract).then(function (data) {

      $scope.contracts = Contracts.getAll();
      var alertPopup = $ionicPopup.alert({
        title: 'Correcto!',
        template: 'Contrato enviado exitosamente',
        okText: 'Aceptar',
        okType: 'button-balanced'
      });

      alertPopup.then(function (res) {
        var user = SessionService.getUser();

        AuthService.getUser(user.id).then(function (response) {
          var userResponse = response.data.response;
          AuthService.getClientById(user.id).then(function (data) {
            var client = data.response[0];
            userResponse.client = client;
            SessionService.setUser(userResponse);
          });

        },function (error) {

        });
        $rootScope.$broadcast('loading:hide');
        $state.go('app.client.contrato');
      });

    }, function (err) {

      var email_existe = err.data && err.data.response && err.data.response.errors && err.data.response.errors['client.email'] ? true : false;

      if (email_existe) {

        var alertPopup = $ionicPopup.alert({
          title: 'Oops!',
          template: 'El email del contrato ya ha sido previamente registrado',
          buttons: [{
            'text': 'Aceptar',
            'type': 'button-assertive',
            'onTap': function (e) {

              $scope.$parent.contractData = contract;
              $scope.$parent.contractFotos = contract.fotos;
            }
          }]
        });

      } else {

        var alertPopup = $ionicPopup.alert({
          title: 'Oops!',
          template: 'Error al enviar el contrato',
          okText: 'Aceptar',
          okType: 'button-assertive'
        });


        var error = err.data ? err.data : err;

        var obj = {
          //'payload': formData,
          'error': error
        };
        $scope.currentUser.notifyError(JSON.stringify(obj));

      }
      $rootScope.$broadcast('loading:hide');
    });
  };

  $scope.changeStep = function (step, next) {
    $scope.stepOne = false;
    $scope.stepTwo = false;
    $scope.errorMaterial = false;
    $ionicScrollDelegate.scrollTop();
    switch (step) {
      case 1:
        if ($scope.contractData.material.id_categories_elements) {
          next ? $scope.stepTwo = true : null;
        } else {
          $scope.errorMaterial = true;
          $scope.stepOne = true;
        }
        break;
      case 2:
        next ? $scope.stepThree = true : $scope.stepOne = true;
        break;
    }
  };

  $scope.toggleResponsabilidad = function () {
    $scope.contractData.client.social_responsability = !$scope.contractData.client.social_responsability;
  };

  $scope.clickExtra = function ($event, extra) {
    $scope.toggleExtra(extra);
  };

  $scope.toggleExtra = function (extra) {
    if ($scope.contractData.elements[extra.id_categories_elements]) {
      delete $scope.contractData.elements[extra.id_categories_elements];
    } else {
      $scope.contractData.elements[extra.id_categories_elements] = extra;
    }
  };

  $scope.verifyExtra = function (extra, category) {
    var extras = $filter('filter')($scope.elementsContract, {
      'category_name': '!Material',
      'element_name': extra.name,
      'category_name': category.name,
    });
    if (extras.length > 0) {
      $scope.contractData.elements[extra.id_categories_elements] = extra;
    }
  };

  $scope.$watch('contractData', function (newVal, OldVal) {

    //console.log(newVal);
    var sum = 0;
    sum += newVal.mensual;
    angular.forEach(newVal.elements, function (value, key) {
      sum += parseFloat(value.price);
    });
    sum += Object.keys(newVal.material).length > 0 ? parseFloat(newVal.material.price) : 0;

    var resp = (sum / 1.16) * 0.007;

    if ($scope.vendedor.sign_into) {
      sum += parseFloat($scope.vendedor.sign_into.deposit) / 100;
    }
    sum += newVal.client.social_responsability ? resp : 0;

    if ($scope.isValidCupon) {
      sum = sum - $scope.descuento;
    }

    if (sum < 0) {
      sum = 0;
    }

    $scope.totalCurrentContract = sum;

  }, true);
}

ClientEditarContratoCtrl.$inject = [
  '$scope',
  'SessionService',
  'Contracts',
  'Coupon',
  'Payment',
  '$cordovaCamera',
  '$cordovaActionSheet',
  '$filter',
  '$ionicPopup',
  '$rootScope',
  '$state',
  'Imagen',
  'AuthService',
  '$ionicHistory',
  'Categories',
  '$ionicScrollDelegate'
];
