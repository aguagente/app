function SellerMainCtrl($scope,$state,$filter,Contracts,$ionicPopup,$cordovaCamera, $cordovaActionSheet, AuthService) {

    $scope.setMenuType("guest");

    $scope.categories = [];
    $scope.elements   = [];
    $scope.fees = {};
    $scope.vendedor = $scope.currentUser.getGroupSeller();

    $scope.relationCard = {
        "amex" : "AMERICAN_EXPRESS",
        "visa" : "VISA",
        "mastercard" : "MC"
    };

    $scope.totalCurrentContract = 0;
    $scope.contractData = {
        "fecha"            : $filter('date')(new Date(), 'yyyy-MM-dd'),
        "material"          : {},
        "elements"           : {},
        "mensual"          : 0,
        "responsabilidad"  : true,
        "monthly_installments" : 1,
        "fee_installments" : 0,
        "credit_card" : {}
    };
    
    $scope.contractFotos = [];

    $scope.popUp = function(msj,success,callback){
        var alertPopup = $ionicPopup.alert({
            title: success ? 'Correcto!'  : 'Ooops!',
            template: msj,
            buttons: [{
                'text' : 'Aceptar',
                'type' : success ? 'button-balanced' : 'button-assertive',
                'onTap' : callback
            }]
        });
    };

    $scope.cameraImage = function(index, type){
        
        var options = {
            quality: 40,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: type,
            allowEdit: false,
            encodingType: Camera.EncodingType.JPEG,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation:true
        };

        $cordovaCamera
            .getPicture(options)
            .then(function(imageData) {
                $scope.contractFotos[index] = 'data:image/jpeg;base64,'+imageData;
            }, function(err) {
                console.log(err);
            });

    };

    $scope.captureImage = function(index, sourceType){
        
        var options = {
            title: 'Metodo para obtener la imagen',
            buttonLabels: ['Carrete/Galeria', 'Hacer foto'],
            addCancelButtonWithLabel: 'Cancelar',
            androidEnableCancelButton : true,
            winphoneEnableCancelButton : true
        };

        var types = {
            1 : Camera.PictureSourceType.PHOTOLIBRARY,
            2 : Camera.PictureSourceType.CAMERA
        };

        $cordovaActionSheet
            .show(options)
            .then(function(btnIndex){

                if(btnIndex == 1 || btnIndex == 2)
                    $scope.cameraImage(index,types[btnIndex]);

            });
    };

    $scope.removeImage = function(index){
    	$scope.contractFotos[index] = null;
    };

    $scope.saveContract = function(){
        $scope.contractData.fecha = $filter('date')(new Date(), 'yyyy-MM-dd');
        var contrato = angular.copy( $scope.contractData );
        contrato.fotos = angular.copy($scope.contractFotos);
        
        if(contrato.id_contrato)
            Contracts.update( contrato );
        else 
            Contracts.append( contrato );

        $state.go('app.seller.contratos', {}, { reload:true });
    };

    $scope.createContract = function(){
        AuthService
            .getClientByEmail( $scope.contractData.email )
            .then(function(response){

                if(response.success && response.response.length){
                    
                    var alertPopup = $ionicPopup.alert({
                        title: 'Oops!',
                        template: 'El email del contrato ya ha sido previamente registrado',
                        buttons: [{
                            'text' : 'Aceptar',
                            'type' : 'button-assertive',
                            'onTap' : function(e){
                                $state.go('app.seller.datos');
                            }
                        }]
                    });             

                }
                else{

                    $scope.saveContract();

                }

            },function(err){

                $scope.saveContract();

            });
    };

    $scope.$watch('contractData', function(newVal,OldVal){
        
        var sum = 0;
        sum += newVal.mensual;
        angular.forEach(newVal.elements, function(value, key) {
            sum += parseFloat(value.price);
        });
        sum += Object.keys(newVal.material).length > 0 ? parseFloat(newVal.material.price) : 0;

        var resp = (sum / 1.16) * 0.007;

        sum += parseFloat( $scope.vendedor.sign_into.deposit ) / 100;
        sum += newVal.responsabilidad ? resp : 0;
        $scope.totalCurrentContract = sum;

    }, true);

}

SellerMainCtrl.$inject = [
    '$scope', '$state', '$filter', 'Contracts', '$ionicPopup', '$cordovaCamera', '$cordovaActionSheet' ,'AuthService'
];