angular.module('aguagenteclient')
.config(function($stateProvider){
	$stateProvider

	.state('app.seller', {
		url      : '/seller',
		abstract : true,
		cache    : false,
		views    : {
			'menuContent@app' : {
				template   : '<ion-view><ion-nav-view name="sellerContent"></ion-nav-view></ion-view>',
				controller : 'SellerMainCtrl'
			}
		}
	})

	.state('app.seller.welcome', {
		cache : true,
		url   : '/welcome',
		views : {
			'sellerContent' : {
				templateUrl : 'app/seller/welcome/welcome.html'
			}
		}
	})

	.state('app.seller.password', {
		cache : false,
		url   : '/password',
		views : {
			'sellerContent' : {
				templateUrl : 'app/password/password.html',
				controller  : 'PasswordCtrl'
			}
		}
	})

	.state('app.seller.cotizador', {
		cache : false,
		url   : '/cotizado',
		views : {
			'sellerContent' : {
				templateUrl : 'app/seller/cotizador/cotizador.html',
				controller  : 'CotizadorCtrl'
			}
		}
	})

	.state('app.seller.encuestas', {
		cache : false,
		url   : '/encuestas',
		views : {
			'sellerContent' : {
				templateUrl : 'app/seller/encuestas/encuestas.html',
				controller  : 'EncuestaListCtrl'
			}
		}
	})

	.state('app.seller.encuesta', {
		cache : false,
		url   : '/encuesta/:id',
		views : {
			'sellerContent' : {
				templateUrl : 'app/seller/encuestas/encuesta.html',
				controller  : 'EncuestaCtrl'
			}
		}
	})

	.state('app.seller.extras', {
		cache : false,
		url   : '/extras',
		views : {
			'sellerContent' : {
				templateUrl : 'app/seller/extras/extras.html',
				controller  : 'ExtraCtrl'
			}
		}
	})

	.state('app.seller.mensual', {
		cache : false,
		url   : '/mensual',
		views : {
			'sellerContent' : {
				templateUrl : 'app/seller/mensual/mensual.html',
				controller  : 'MensualCtrl'
			}
		}
	})

	.state('app.seller.fotos', {
		cache : false,
		url   : '/fotos',
		views : {
			'sellerContent' : {
				templateUrl : 'app/seller/fotos/fotos.html'
			}
		}
	})

	.state('app.seller.datos', {
		cache : false,
		url   : '/datos',
		views : {
			'sellerContent' : {
				templateUrl : 'app/seller/datos/datos.html'
			}
		}
	})

	.state('app.seller.tarjeta', {
		cache : false,
		url   : '/tarjeta',
		views : {
			'sellerContent' : {
				templateUrl : 'app/seller/tarjeta/tarjeta.html',
				controller  : 'SellerTarjetaCtrl'
			}
		}
	})

	.state('app.seller.preview', {
		cache : false,
		url   : '/preview',
		views : {
			'sellerContent' : {
				templateUrl : 'app/seller/preview/preview.html',
				controller  : 'PreviewCtrl'
			}
		}
	})

	.state('app.seller.contratos', {
		cache : false,
		url   : '/contratos',
		views : {
			'sellerContent' : {
				templateUrl : 'app/seller/contratos/contratos.html',
				controller  : 'ContratosCtrl'
			}
		}
	})
})