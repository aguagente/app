function ContractMainCtrl($scope,$state,$filter,Contracts,$ionicPopup,$cordovaCamera, $cordovaActionSheet, AuthService, Imagen,$rootScope) {

    $scope.setMenuType("guest");

    $scope.categories = [];
    $scope.elements   = [];
    $scope.fees = {};
    $scope.vendedor = $scope.currentUser.getUser().client.group;//$scope.currentUser.getGroup();

    $scope.relationCard = {
        "amex" : "AMERICAN_EXPRESS",
        "visa" : "VISA",
        "mastercard" : "MC"
    };

    $scope.totalCurrentContract = 0;
    $scope.contractData = {
        "_method" : "PUT",
        "fecha" : $filter('date')(new Date(), 'yyyy-MM-dd'),
        "material" : {},
        "elements" : {},
        "mensual"  : 0,
        "responsabilidad"  : true,
        "monthly_installments" : 1,
        "fee_installments" : 0,
        "credit_card" : {},
        "completar" : true
    };

    $scope.contractFotos = [];


    Contracts
        .getByClient( $scope.currentUser.getIdClient() )
        .then(function (response){

            Contracts
                .getInvalidations( $scope.currentUser.getIdClient() )
                .then(function(data){

                    response[0].invalidations = data;
                    var contrato = response[0];
                    $scope.invalidation = angular.isDefined(contrato.invalidations) ? contrato.invalidations[contrato.invalidations.length - 1] : {};

                    $scope.contractData.id_contracts = contrato.id_contracts;
                    $scope.contractData.monthly_installments = contrato.client.monthly_installments !== null ? contrato.client.monthly_installments : 1;
                    $scope.contractData.name = contrato.client.name;
                    $scope.contractData.email = contrato.client.email;
                    $scope.contractData.phone = contrato.client.phone;
                    $scope.contractData.address = contrato.client.address;
                    $scope.contractData.between_streets = contrato.client.between_streets;
                    $scope.contractData.colony = contrato.client.colony;
                    $scope.contractData.county = contrato.client.county;
                    $scope.contractData.state = contrato.client.state;
                    $scope.contractData.postal_code = contrato.client.postal_code;
                    $scope.contractData.completar = contrato.client.social_responsability !== null ? false : true;
                    $scope.contractData.cupon = contrato.cupon ? contrato.cupon : '';
                    $scope.contractData.material = contrato.client.social_responsability !== null ? $filter('filter')(contrato.elements,{'category_name': 'Material'})[0] : {};
                    $scope.contractData.responsabilidad = contrato.client.social_responsability !== null ? contrato.client.social_responsability : true;
                    $scope.contractData.elems = contrato.elements;

                    angular.forEach(contrato.documents, function(value, key){
                        if( value.search("proof_of_address") !== -1 )
                            Imagen.urlToBase64(value).then(function(base64){
                                $scope.contractFotos[6] = base64;
                            });
                        else if( value.search("profile") !== -1 )
                            Imagen.urlToBase64(value).then(function(base64){
                                $scope.contractFotos[7] = base64;
                            });
                        else if( value.search("id_reverse") !== -1 )
                            Imagen.urlToBase64(value).then(function(base64){
                                $scope.contractFotos[8] = base64;
                            });
                        else if( value.search("id") !== -1 )
                            Imagen.urlToBase64(value).then(function(base64){
                                $scope.contractFotos[9] = base64;
                            });
                    });

                    angular.forEach(contrato.photos, function(value, key){

                        if( value.search("image0") !== -1 ){
                            Imagen.urlToBase64(value).then(function(base64){
                                $scope.contractFotos[0] = base64;
                            });
                        }
                        else if( value.search("image1") !== -1 ){
                            Imagen.urlToBase64(value).then(function(base64){
                                $scope.contractFotos[1] = base64;
                            });
                        }
                        else if( value.search("image2") !== -1 )
                            Imagen.urlToBase64(value).then(function(base64){
                                $scope.contractFotos[2] = base64;
                            });
                        else if( value.search("image3") !== -1 )
                            Imagen.urlToBase64(value).then(function(base64){
                                $scope.contractFotos[3] = base64;
                            });
                        else if( value.search("image4") !== -1 )
                            Imagen.urlToBase64(value).then(function(base64){
                                $scope.contractFotos[4] = base64;
                            });
                        else if( value.search("image5") !== -1 )
                            Imagen.urlToBase64(value).then(function(base64){
                                $scope.contractFotos[5] = base64;
                            });

                    });

                });

        });

    $scope.contractFotos = [];

    $scope.popUp = function(msj,success,callback){
        var alertPopup = $ionicPopup.alert({
            title: success ? 'Correcto!'  : 'Ooops!',
            template: msj,
            buttons: [{
                'text' : 'Aceptar',
                'type' : success ? 'button-balanced' : 'button-assertive',
                'onTap' : callback
            }]
        });
    };

    $scope.cameraImage = function(index, type){

        var options = {
            quality: 40,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: type,
            allowEdit: false,
            encodingType: Camera.EncodingType.JPEG,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation:true
        };

        $cordovaCamera
            .getPicture(options)
            .then(function(imageData) {
                $scope.contractFotos[index] = 'data:image/jpeg;base64,'+imageData;
            }, function(err) {
                console.log(err);
            });

    };

    $scope.captureImage = function(index, sourceType){

        var options = {
            title: 'Metodo para obtener la imagen',
            buttonLabels: ['Carrete/Galeria', 'Hacer foto'],
            addCancelButtonWithLabel: 'Cancelar',
            androidEnableCancelButton : true,
            winphoneEnableCancelButton : true
        };

        var types = {
            1 : Camera.PictureSourceType.PHOTOLIBRARY,
            2 : Camera.PictureSourceType.CAMERA
        };

        $cordovaActionSheet
            .show(options)
            .then(function(btnIndex){

                if(btnIndex == 1 || btnIndex == 2)
                    $scope.cameraImage(index,types[btnIndex]);

            });
    };

    $scope.removeImage = function(index){
        $scope.contractFotos[index] = null;
    };

    $scope.datosConekta = function(data,name){
        return  {
            "name"      : name,
            "number"    : data.card_number,
            "cvc"       : data.cvc,
            "exp_month" : data.expiration_month,
            "exp_year"  : data.expiration_year
        };
    };

    $scope.enviarContrato = function(formData){

        Contracts.toCorrect(formData,$scope.contractData.id_contracts)
            .then(function(data){

            var alertPopup = $ionicPopup.alert({
               title: 'Correcto!',
               template: 'Contrato enviado exitosamente',
               okText: 'Aceptar',
               okType: 'button-balanced'
            });

            alertPopup.then(function(res){
                $state.go('app.client.contrato');
            });

        }, function(err){

            var alertPopup = $ionicPopup.alert({
               title: 'Oops!',
               template: 'Error al enviar el contrato',
               okText: 'Aceptar',
               okType: 'button-assertive'
            });


            var error = err.data ? err.data : err;
            var obj = {
                'payload' : formData,
                'error' : error
            };
            $scope.currentUser.notifyError(JSON.stringify(obj));

        });
    };

    $scope.createContract = function(){

        var contract = $scope.contractData;
        var confirmPopup = $ionicPopup.confirm({
            title      : 'Desea continuar?',
            template   : 'Seguro que desea enviar el contrato?',
            cancelText : 'Cancelar',
            okText     : 'Aceptar'
        });


        confirmPopup.then(function(res){
            if(res){

                $rootScope.$broadcast('loading:show');

                var pictures = {
                    6 : "proof_of_address",
                    7 : "id",
                    8 : "id_reverse",
                    9 : "profile"
                };


                var formData = {
                    "_method" : 'PUT',
                    "id_contracts" : contract.id_contracts,
                    "cupon" : contract.cupon,
                    "client" : {
                        "name" : contract.name,
                        "address" : contract.address,
                        "phone" : contract.phone,
                        "between_streets" : contract.between_streets,
                        "colony" : contract.colony,
                        "state" : contract.state,
                        "county" : contract.county,
                        "postal_code" : contract.postal_code,
                        "responsabilidad" : contract.responsabilidad,
                        "id_groups" : $scope.vendedor.id_groups
                    },
                    "monthly_installments" : contract.monthly_installments,
                    "upload_documents" : {},
                    "upload_photos" : {},
                    "elems" : []
                };

                for(var i=0; i<$scope.contractFotos.length; i++){

                    var blob = Imagen.base64ToBlob( $scope.contractFotos[i] );
                    if(blob){
                        if(i < 6)
                            formData.upload_photos['image'+i] = blob;
                        else
                            formData.upload_documents[pictures[i]] = blob;
                    }

                }

                formData.elems.push(contract.material.id_categories_elements);
                angular.forEach(contract.elements, function(value,key){
                    formData.elems.push( key );
                });

                /*var formData = new FormData();

                formData.append('_method', 'PUT');
                formData.append('id_contracts', contract.id_contracts);
                formData.append('client[name]', contract.name);
                formData.append('client[address]', contract.address);
                formData.append('client[phone]', contract.phone);
                formData.append('client[email]', contract.email);
                formData.append('client[between_streets]', contract.between_streets);
                formData.append('client[colony]', contract.colony);
                formData.append('client[state]', contract.state);
                formData.append('client[county]', contract.county);
                formData.append('client[postal_code]', contract.postal_code);
                formData.append('client[id_groups]', $scope.vendedor.id_groups );
                formData.append('responsabilidad', contract.responsabilidad );
                //formData.append('seller', contract.seller );
                formData.append('monthly_installments', contract.monthly_installments );
                formData.append('elements[]', contract.material.id_categories_elements );

                angular.forEach(contract.elements, function(value,key){
                    formData.append( 'elements[]', key );
                });

                for(var i=0; i<$scope.contractFotos.length; i++){

                    var blob = Imagen.base64ToBlob( $scope.contractFotos[i] );
                    if(blob){
                        var imageName = 'image'+i+'.'+(blob.mime.replace('image/',''));
                        var name = i < 6 ? 'photos[]' : pictures[i];
                        formData.append( name, blob.file, imageName );
                    }

                }*/

                if(contract.credit_card && contract.credit_card.card_number){
                    var datos = $scope.datosConekta(contract.credit_card, contract.name);
                    Conekta.token.create({"card":datos},
                        function(token){
                            formData.credit_card = token.id;
                            //formData.append('credit_card', token.id );
                            $scope.enviarContrato(formData);
                        },
                        function(response){
                            $scope.popUp('Error al guardar la tarjeta en conekta, verifica que los datos de la tarjeta sean los correctos',false,null);
                            $scope.currentUser.notifyError(JSON.stringify(response));
                        }
                    );
                }
                else{
                    $scope.enviarContrato(formData);
                }

            }
        });

    };

    $scope.$watch('contractData', function(newVal,OldVal){

        var sum = 0;
        sum += newVal.mensual;
        angular.forEach(newVal.elements, function(value, key) {
            sum += parseFloat(value.price);
        });
        sum += Object.keys(newVal.material).length > 0 ? parseFloat(newVal.material.price) : 0;

        var resp = (sum / 1.16) * 0.007;

        sum += parseFloat( $scope.vendedor.sign_into.deposit ) / 100;
        sum += newVal.responsabilidad ? resp : 0;

        $scope.totalCurrentContract = sum;

    }, true);

}

ContractMainCtrl.$inject = [
    '$scope', '$state', '$filter', 'Contracts', '$ionicPopup', '$cordovaCamera', '$cordovaActionSheet' ,'AuthService', 'Imagen','$rootScope'
];
