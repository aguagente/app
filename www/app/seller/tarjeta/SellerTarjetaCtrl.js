function SellerTarjetaCtrl($scope, $state, Payment, $ionicPopup) {

	$scope.tarjeta = angular.copy($scope.contractData.credit_card);
	$scope.fee = $scope.tarjeta.bank ? $scope.fees[$scope.relationCard[$scope.tarjeta.bank]] : null;

	Payment.getFees().then(function(response){

		$scope.$parent.fees = response;
		$scope.fee = $scope.tarjeta.bank ? $scope.fees[$scope.relationCard[$scope.tarjeta.bank]] : null;

    });

	$scope.validarTarjeta = function(data){
		if( !Conekta.card.validateNumber(data.card_number) ) return 'Número de tarjeta invalido';
		if( !Conekta.card.validateCVC(data.cvc) ) return 'CVC incorrecto';
		if( !Conekta.card.validateExpirationDate(data.expiration_month,data.expiration_year) ) return 'Fecha de vencimiento invalida';
		if( !data.bank ) return 'Tipo de tarjeta obligatorio';
		if( Conekta.card.getBrand( data.card_number.toString() ) != data.bank ) return 'Tipo de tarjeta incorrecto';
		return false;
	};

	$scope.changeBrand = function(){
		if($scope.tarjeta.bank){

			$scope.fee = $scope.fees[$scope.relationCard[$scope.tarjeta.bank]];
			$scope.contractData.fee_installments = parseFloat($scope.fees[$scope.relationCard[$scope.tarjeta.bank]][$scope.contractData.monthly_installments]);

		}
		else
			$scope.fee = null;
	};

	$scope.changeFormaPago = function(){

		$scope.contractData.fee_installments = parseFloat($scope.fees[$scope.relationCard[$scope.tarjeta.bank]][$scope.contractData.monthly_installments]);

	};


	$scope.enviar = function(){

		var error = $scope.validarTarjeta($scope.tarjeta);

		if(error){
			$scope.popUp(error,false,null);
		}
		else{

			$scope.contractData.credit_card = angular.copy($scope.tarjeta);
			if($state.includes('app.seller'))
				$state.go("app.seller.preview");
			else
				$state.go("app.client.contract.preview");

		}
		return;
	};

}

SellerTarjetaCtrl.$inject = [
    '$scope', '$state', 'Payment', '$ionicPopup'
];