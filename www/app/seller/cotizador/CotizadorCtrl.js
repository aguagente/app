function CotizadorCtrl($scope,$filter,Categories,Elements) {

    $scope.group = $scope.currentUser.getGroupSeller();
    $scope.deposito = parseFloat($scope.vendedor.sign_into.deposit) / 100;

	$scope.changeMaterial = function(elem){
        $scope.contractData.material = elem;
    };

	Categories.getAll().then(function(response){

        $scope.$parent.categories = response;

        $scope.material = $filter('filter')(response,{'name':'Material'})[0];
        if($scope.material){
            var acero = $filter('filter')($scope.material.elements,{'name':'Acero inoxidable'})[0];
            if(acero){
                acero.price = parseFloat( $scope.vendedor.sign_into.installation_fee ) / 100;
            }
        }

		var element = $scope.material ? $scope.material.elements[0] : {};
        if(Object.keys($scope.contractData.material).length > 0){

            if($scope.contractData.material.id_categories_elements)
        	    var elegido = $filter('filter')($scope.material.elements,{'id_categories_elements': $scope.contractData.material.id_categories_elements});
            else{
                var elegido = $filter('filter')($scope.material.elements,{'name': $scope.contractData.material.element_name});

            }
        	element = elegido ? elegido[0] : element;

        }
		$scope.changeMaterial(element);

    });

}

CotizadorCtrl.$inject = [
    '$scope','$filter', 'Categories', 'Elements'
];