function SellerWelcomeCtrl($scope) {

	$scope.android = ionic.Platform.isAndroid();
	$scope.src = 'aguagente_sub';
	$scope.subtitles = false;

    $scope.reproducirVideo = function(){
        VideoPlayer.play("file:///android_asset/www/img/seller/"+$scope.src+".mp4");
    };

    $scope.toggleVideo = function(){
    	$scope.src = $scope.src == 'aguagente' ? 'aguagente_sub' : 'aguagente';
    };

    $scope.$watch('subtitles', function(newVal,OldVal){
    	$scope.toggleVideo();
    });

}

SellerWelcomeCtrl.$inject = [
    '$scope'
];