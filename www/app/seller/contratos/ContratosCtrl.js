function ContratosCtrl($scope,$ionicPopup,$ionicPlatform,$rootScope,$state,Contracts,Imagen) {

	$scope.contracts = [];

	$ionicPlatform.registerBackButtonAction(function(e) {
        e.preventDefault();
    }, 101);

    $scope.$watch( function(){ 
			return Contracts.getAll();
		}, 

		function(newVal){ 
			$scope.contracts = newVal;
	}, true);

	$scope.eliminar = function(name, indice){

		var confirmPopup = $ionicPopup.confirm({
			title: 'Desea continuar?',
			template: 'Seguro que desea eliminar el contrato '+name+'?',
			cancelText : 'Cancelar',
			okText     : 'Eliminar',
			okType     : 'button-assertive'
		});

		confirmPopup.then(function(res) {
			if(res) {
				Contracts.delete(indice);
				$scope.contracts = Contracts.getAll();
			}
		});
		
	};

	$scope.datosConekta = function(data,name){
		return  {
			"name"      : name,
			"number"    : data.card_number,
			"cvc"       : data.cvc,
			"exp_month" : data.expiration_month,
			"exp_year"  : data.expiration_year
		};
	};

	$scope.editar = function(contract,indice){
		$scope.$parent.contractData = contract;
		$scope.$parent.contractFotos = contract.fotos;
        $state.go('app.cotizador.start');
	};

	$scope.enviarContrato = function(contract,indice,formData){
		Contracts.send(formData).then(function(data){

			Contracts.delete(indice);
			$scope.contracts = Contracts.getAll();
			var alertPopup = $ionicPopup.alert({
		       title: 'Correcto!',
		       template: 'Contrato enviado exitosamente',
		       okText: 'Aceptar',
		       okType: 'button-balanced'
		    });

		}, function(err){

			var email_existe = err.data && err.data.response && err.data.response.errors && err.data.response.errors['client.email'] ? true : false;

			if(email_existe){

				var alertPopup = $ionicPopup.alert({
                    title: 'Oops!',
                    template: 'El email del contrato ya ha sido previamente registrado',
                    buttons: [{
                        'text' : 'Aceptar',
                        'type' : 'button-assertive',
                        'onTap' : function(e){

							$scope.$parent.contractData = contract;
							$scope.$parent.contractFotos = contract.fotos;
					        $state.go('app.seller.datos');

                        }
                    }]
                }); 

			}
			else{

				var alertPopup = $ionicPopup.alert({
			       title: 'Oops!',
			       template: 'Error al enviar el contrato',
			       okText: 'Aceptar',
			       okType: 'button-assertive'
			    });


				var error = err.data ? err.data : err;

				var obj = {
	                'payload' : formData,
	                'error' : error
	            };
            	$scope.currentUser.notifyError(JSON.stringify(obj));

			}

		});
	};

	$scope.enviar = function(contract, indice){

		var confirmPopup = $ionicPopup.confirm({
			title      : 'Desea continuar?',
			template   : 'Seguro que desea enviar el contrato?',
			cancelText : 'Cancelar',
			okText     : 'Aceptar'
		});


		confirmPopup.then(function(res){
			if(res){

				$rootScope.$broadcast('loading:show');

				var pictures = {
					6 : "proof_of_address",
					7 : "id",
					8 : "id_reverse",
					9 : "profile"
				};

				var formData = new FormData();
				
				formData.append('client[name]', contract.name);
				formData.append('client[address]', contract.address);
				formData.append('client[phone]', contract.phone);
				formData.append('client[email]', contract.email);
				formData.append('client[between_streets]', contract.between_streets);
				formData.append('client[colony]', contract.colony);
				formData.append('client[state]', contract.state);
				formData.append('client[county]', contract.county);
				formData.append('client[postal_code]', contract.postal_code);
				formData.append('client[id_groups]', $scope.currentUser.getGroupSeller().id_groups );
				formData.append('responsabilidad', contract.responsabilidad );
				formData.append('seller', contract.seller );
				formData.append('cupon', contract.cupon );
				formData.append('monthly_installments', contract.monthly_installments );
				formData.append('elements[]', contract.material.id_categories_elements );

				angular.forEach(contract.elements, function(value,key){
					formData.append( 'elements[]', key );
				});

				for(var i=0; i<contract.fotos.length; i++){

					var blob = Imagen.base64ToBlob( contract.fotos[i] );
					if(blob){
						var imageName = 'image'+i+'.'+(blob.mime.replace('image/',''));
						var name = i < 6 ? 'photos[]' : pictures[i];
						formData.append( name, blob.file, imageName );
					}
					
				}

				if(contract.credit_card && contract.credit_card.card_number){
					var datos = $scope.datosConekta(contract.credit_card, contract.name);
					Conekta.token.create({"card":datos}, 
						function(token){
							formData.append('credit_card', token.id );
							$scope.enviarContrato(contract, indice, formData);
						}, 
						function(response){
							$scope.popUp('Error al guardar la tarjeta en conekta, verifica que los datos de la tarjeta sean los correctos',false,null);
							$scope.currentUser.notifyError(JSON.stringify(response));
						}
					);
				}
				else{
					$scope.enviarContrato(contract, indice, formData);
				}	
				
			}
		});

	};
}

ContratosCtrl.$inject = [
    '$scope','$ionicPopup','$ionicPlatform','$rootScope','$state','Contracts','Imagen'
];