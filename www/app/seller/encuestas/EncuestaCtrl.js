function EncuestaCtrl($scope, $state, $stateParams, $ionicPopup, Poll, $cordovaActionSheet) {

	$scope.registro = {};
	$scope.contestadas = [];

    Poll.show($stateParams.id).then(function(response){
        $scope.registro = response;
    });

    $scope.responder = function(preg,resp){

    	if( preg.answers[resp.id_options] ){
    		delete preg.answers[resp.id_options];
    	}
    	else{
    		if(preg.type == "single")
    			preg.answers = {};
    		preg.answers[resp.id_options] = resp;
    	}

    };

    $scope.enviarEncuesta = function(){
    	
    	var payload = [];

    	var todosRespondidos = true;

    	angular.forEach($scope.registro.questions, function(value,key){

    		var options = [];

    		angular.forEach(value.answers, function(value, key){
    			this.push(key);
    		}, options);

    		todosRespondidos = todosRespondidos && options.length > 0;

    		this.push({
    			"id_questions" : value.id_questions,
    			"options" : options
    		});

    	}, payload);

    	if(todosRespondidos){
	    	Poll
	    		.answer( $stateParams.id, {"answers":payload} )
	    		.then(function(response){

	    			var alertPopup = $ionicPopup.alert({
						title    : 'Correcto!',
						template : 'Encuesta contestada exitosamente',
						buttons  : [{
							'text'  : 'Aceptar',
							'type'  : 'button-balanced',
							'onTap' : function(e){

								$state.transitionTo("app.seller.encuestas", null, {
						            reload: true,
						            inherit: true,
						            notify: true
						        });

							}
						}]
				    });

	    		}, function(err){

	    			var alertPopup = $ionicPopup.alert({
				       title: 'Oops!',
				       template: 'Error al enviar la encuesta',
				       okText: 'Aceptar',
				       okType: 'button-assertive'
				    });

	    		});
    	}
	    else{

	    	var alertPopup = $ionicPopup.alert({
		       title: 'Oops!',
		       template: 'Todas las preguntas deben ser contestadas',
		       okText: 'Aceptar',
		       okType: 'button-assertive'
		    });
		    
	    }


    };

}

EncuestaCtrl.$inject = [
    '$scope', '$state', '$stateParams', '$ionicPopup' ,'Poll', '$cordovaActionSheet'
];