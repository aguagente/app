function EncuestaListCtrl($scope, $state, Poll) {

	$scope.polls = [];

    Poll.getByClient().then(function(response){
        $scope.polls = response;
    });

}

EncuestaListCtrl.$inject = [
    '$scope', '$state', 'Poll'
];