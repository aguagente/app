function MensualCtrl($scope) {

	$scope.contractData.mensual = parseFloat( $scope.vendedor.sign_into.monthly_fee ) / 100;

	$scope.toggleResponsabilidad = function(){
		$scope.contractData.responsabilidad = !$scope.contractData.responsabilidad;
	};

}

MensualCtrl.$inject = [
    '$scope'
];