function ExtraCtrl($scope,$filter,$ionicModal) {

	$scope.image = null;

	if( $scope.contractData.elems && $scope.contractData.elems.length ){
		
	    angular.forEach($scope.contractData.elems, function(value,key){
	    	if(value.category_name != 'Material'){
	    		var elems = $filter('filter')( $scope.$parent.categories, {'name': value.category_name} )[0];
	    		var elem = $filter('filter')( elems.elements, {'name': value.element_name} );
		        if(elem)
		            $scope.contractData.elements[elem[0].id_categories_elements] = elem[0];
		    }
	    });

	}

	$scope.toggleExtra = function(extra){

		if( $scope.contractData.elements[extra.id_categories_elements] ){
			delete $scope.contractData.elements[extra.id_categories_elements];
		}
		else{
			$scope.contractData.elements[extra.id_categories_elements] = extra;
		}

	};

	$scope.clickExtra = function($event,extra){

		if(angular.element(event.target).hasClass('ion-image')){
			$scope.image = 'data:'+extra.type+';base64,'+extra.image;
			$scope.modal.show();
		}
		else{
			$scope.toggleExtra(extra);
		}

	};


	$ionicModal.fromTemplateUrl('app/modals/showImage.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.modal = modal;
	});


	$scope.closeModal = function() {
		$scope.image = null;
		$scope.modal.hide();
	};

	$scope.$on('$destroy', function() {
		$scope.modal.remove();
	});

}

ExtraCtrl.$inject = [
    '$scope','$filter','$ionicModal'
];