function NotificacionesCtrl($scope,Notificacion) {

    var user = $scope.currentUser.getUser();
    $scope.notificaciones = [];

    Notificacion
        .get({
            //'platform' : (ionic.Platform.isIOS() ? 'ios' : 'android'),
            'id_users' : user.id
        })
        .then(function(res){

            $scope.notificaciones = res;

        });

}

NotificacionesCtrl.$inject = [
    '$scope','Notificacion'
];