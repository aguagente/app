angular
    .module('aguagenteclient-controllers', [])

.controller('MainController', MainController)
    .controller('LoginController', LoginController)
    .controller('PasswordCtrl', PasswordCtrl)
    .controller('NotificacionesCtrl', NotificacionesCtrl)

.controller('GuestMainCtrl', GuestMainCtrl)
    .controller('HomeCtrl', HomeCtrl)
    .controller('AguagratisCtrl', AguagratisCtrl)
    .controller('SingleCtrl', SingleCtrl)
    .controller('ReportCtrl', ReportCtrl)

.controller('CotizadorMainCtrl', CotizadorMainCtrl)
    .controller('CotizadorOpcionesCtrl', CotizadorOpcionesCtrl)
    .controller('PagoCtrl', PagoCtrl)
    .controller('PagoPreviewCtrl', PagoPreviewCtrl)


.controller('ClientMainCtrl', ClientMainCtrl)
    .controller('ClientContratoCtrl', ClientContratoCtrl)
    .controller('CorreccionCtrl', CorreccionCtrl)
    .controller('ClientTarjetaCtrl', ClientTarjetaCtrl)
    .controller('ClientTarjetaListCtrl', ClientTarjetaListCtrl)
    .controller('MediaCtrl', MediaCtrl)
    .controller('ContractMainCtrl', ContractMainCtrl)
    .controller('ChargesCtrl', ChargesCtrl)
    .controller('ContactosCtrl', ContactosCtrl)

.controller('SellerMainCtrl', SellerMainCtrl)
    .controller('SellerWelcomeCtrl', SellerWelcomeCtrl)
    .controller('CotizadorCtrl', CotizadorCtrl)
    .controller('ExtraCtrl', ExtraCtrl)
    .controller('MensualCtrl', MensualCtrl)
    .controller('ContratosCtrl', ContratosCtrl)
    .controller('EncuestaListCtrl', EncuestaListCtrl)
    .controller('EncuestaCtrl', EncuestaCtrl)
    .controller('PreviewCtrl', PreviewCtrl)
    .controller('SellerTarjetaCtrl', SellerTarjetaCtrl)

.controller('ClientEditarContratoCtrl', ClientEditarContratoCtrl)