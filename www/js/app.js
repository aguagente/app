angular.module('aguagenteclient', [
  'ionic',
  'ngResource',
  'ngSanitize',
  'ngMessages',
  'ngRoute',
  'ngCordova',
  'youtube-embed',
  'jett.ionic.content.banner',
  'aguagenteclient-controllers',
  'aguagenteclient-services',
  'aguagenteclient-constants',
  'aguagenteclient-directives',
  'aguagenteclient-filters'
])

  .run(function ($ionicPlatform, $state, $ionicLoading, $ionicPopup, $rootScope, AuthService, Contracts, Categories, $interval, $cordovaNetwork, $ionicContentBanner, $cordovaDialogs, $cordovaGoogleAnalytics, $ionicModal, $cordovaSocialSharing, SessionService, $http, API, $window) {
    $rootScope.$state = $state;
    $rootScope.isJwt = API.isJwt;
  

    if ($rootScope.isJwt) {
      var accessToken = SessionService.getAccessToken();
      if (accessToken) {
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
      }
    } else {
      AuthService.getToken();
      $interval(function () {
        AuthService.getToken();
      }, 6000000);
    }

    $ionicPlatform.ready(function () {
      if (!$rootScope.isJwt) {
        AuthService.getToken();
      }
      var isOnline = true; //$cordovaNetwork.isOnline();

      //console.log(apiAguagente.environment);
      Conekta.setPublishableKey(apiAguagente.environment === 'production' ? "key_TGFAZrzeFTSi71VbMPfmCkg" : 'key_MaFzT3SvrAEBkonXV3gg2HQ'); // Produccion
      var openPayEnvironment = apiAguagente.openPayEnvironment == 'sandbox';


      var d = new Date();
    
    $http.get('https://www.aguagente.com/config/config.json' + '?d=' + d, {withCredentials: false}).then(function (response) {
      var configurations = response.data;
        if (apiAguagente.environment === 'production') {
          OpenPay.setId(configurations.prod.client.keys.openpay.id);
          OpenPay.setApiKey(configurations.prod.client.keys.openpay.publick_key);
          Conekta.setPublishableKey(configurations.prod.client.keys.conekta);
        } else {
          OpenPay.setId(configurations.demo.client.keys.openpay.id);
          OpenPay.setApiKey(configurations.demo.client.keys.openpay.publick_key);
          OpenPay.setSandboxMode(configurations.environment === 'production');
          Conekta.setPublishableKey(configurations.demo.client.keys.conekta);
        }
        console.log(OpenPay.getApiKey());
      }, function (error) { })


      //Conekta.setPublishableKey("key_MaFzT3SvrAEBkonXV3gg2HQ"); //Beta
      //$cordovaGoogleAnalytics.startTrackerWithId('UA-88406110-1');
      //$cordovaGoogleAnalytics.startTrackerWithId('UA-141875574-1');
      var push = PushNotification.init({
        android: {
          senderID: "490235713718",
          clearBadge: true
        },
        ios: {
          alert: true,
          badge: false,
          sound: true,
          clearBadge: true
        }
      });

      push.on('registration', function (data) {
        AuthService.setPushToken(data.registrationId);
        console.log("Push notification registration: ", data.registrationId);
      });

      push.on('error', function (e) {
        console.log("Push notification error: ", e.message);
      });

      push.on('notification', function (data) {

        $cordovaDialogs.alert(data.message, 'Aguagente', 'Aceptar')
          .then(function () {
            // callback success
          });
      });

      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
        cordova.plugins.Keyboard.disableScroll(true);
      }

      if (window.StatusBar) {
        StatusBar.styleDefault();
      }

      if (window.cordova) {

        /*if(window.codePush && isOnline){

                  window.codePush.sync(function(status){

                      switch (status) {
                          case SyncStatus.DOWNLOADING_PACKAGE:
                              $ionicLoading.show({showBackdrop: true});
                              break;
                          case SyncStatus.INSTALLING_UPDATE:
                              $ionicLoading.hide();
                              $ionicPopup.alert({
                                  title: 'Precaución!',
                                  template: 'Es necesario reiniciar tu app para aplicar actualizaciones realizadas'
                              });
                              break;
                      }
                  }, { installMode: InstallMode.ON_NEXT_RESTART, minimumBackgroundDuration: 120, mandatoryInstallMode: InstallMode.ON_NEXT_RESTART }, null);

              }*/

        var db = window.sqlitePlugin.openDatabase({ name: "contratos.db", location: 2 },
          function (data) {
            console.log('BD BIEN');
          },
          function (err) {
            console.log('BD MAL ' + err.message);
          });
      } else
        var db = window.openDatabase("contratos", "1.0", "Contratos aguagente", 100000000);

      db.transaction(function (tx) {

        /*tx.executeSql("DROP TABLE IF EXISTS contracts");
              tx.executeSql("DROP TABLE IF EXISTS categories");*/
        tx.executeSql("DROP TABLE IF EXISTS categories_elements");

        tx.executeSql("CREATE TABLE IF NOT EXISTS contracts (id_contrato VARCHAR PRIMARY KEY NOT NULL, cliente INTEGER NOT NULL, contrato TEXT NOT NULL)");
        tx.executeSql("CREATE TABLE IF NOT EXISTS categories (id_categories INT PRIMARY KEY NOT NULL, name VARCHAR NOT NULL, description TEXT NOT NULL, elements TEXT)");
        //tx.executeSql("CREATE TABLE IF NOT EXISTS categories_elements (id_categories_elements INT PRIMARY KEY NOT NULL, id_categories INT NOT NULL, name VARCHAR NOT NULL, price REAL NOT NULL, image TEXT, type VARCHAR DEFAULT NULL)");

        tx.executeSql("SELECT contrato FROM contracts", [], function (tx, res) {
          var contratos = [];
          for (var i = 0; i < res.rows.length; i++) {
            var reg = angular.fromJson(res.rows.item(i).contrato);
            contratos.push(reg);
          }
          Contracts.setContracts(contratos);
        });

        tx.executeSql("SELECT * FROM categories", [], function (tx, res) {

          var categories = [];
          for (var i = 0; i < res.rows.length; i++) {
            var reg = res.rows.item(i);
            reg.elements = angular.fromJson(reg.elements);
            categories.push(reg);
          }

          Categories.setCategories(categories);

        });


      }, function (error) {
        console.log('transaction error: ' + error.message);
      }, function () {
        console.log('transaction ok');
      });

      Contracts.setDataBase(db);
      Categories.setDataBase(db);

    });

    $rootScope.errModalTxt = 'Internal server error';
    var htmlModal = '<ion-modal-view><ion-header-bar class="bar bar-header bar-assertive"><h1 class="title">Oops!</h1><button class="button button-clear button-primary" ng-click="errorModal.hide()">Cerrar</button></ion-header-bar><ion-content><div ng-bind-html="errModalTxt"></div></ion-content></ion-modal-view>';
    $rootScope.errorModal = $ionicModal.fromTemplate(htmlModal, {
      scope: $rootScope,
      animation: 'slide-in-up'
    });

    $rootScope.popupVideoHide = false;
    var _playOnopenLocalStorage = window.localStorage['playvideoonopen'] ? JSON.parse(window.localStorage['playvideoonopen']) : true;

    if (!_playOnopenLocalStorage) {
      $rootScope.popupVideoHide = true;
    }


    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {

      var str = (toState.name).replace('app.', '').replace(/\./g, ' ');
      //$cordovaGoogleAnalytics.trackView(str);

    });

    $rootScope.$on('token', function () {
      if (API.isJwt) {
        AuthService.refreshToken();
      } else {
        AuthService.getToken();
      }

    });

    $rootScope.$on('errorHttp:show', function () {
      $rootScope.errorModal.show();
    });

    $rootScope.$on('loading:show', function () {
      $ionicLoading.show({ showBackdrop: true });
    });

    $rootScope.$on('loading:hide', function () {
      $ionicLoading.hide();
    });

    $rootScope.$on('$ionicView.afterEnter', function () {

      if ($cordovaNetwork.isOnline() && angular.isDefined($rootScope.closeNetworkErrorBanner)) {

        $rootScope.closeNetworkErrorBanner();
        $rootScope.closeNetworkErrorBanner = undefined;

      } else if ($cordovaNetwork.isOffline()) {

        $rootScope.closeNetworkErrorBanner = $ionicContentBanner.show({
          text: [
            'No hay conexión a internet'
          ],
          interval: 3000,
          type: 'error'
        });
      }
    });

    $rootScope.$on('$cordovaNetwork:online', function (event) {

      if (angular.isDefined($rootScope.closeNetworkErrorBanner))
        $rootScope.closeNetworkErrorBanner();

    });

    $rootScope.$on('$cordovaNetwork:offline', function (event) {

      $rootScope.closeNetworkErrorBanner = $ionicContentBanner.show({
        text: [
          'No hay conexión a internet'
        ],
        interval: 3000,
        type: 'error'
      });

    });

    $rootScope.$on('logoutUser', function (event) {
      if ($rootScope.isJwt) {
        $http.defaults.headers.common['Authorization'] = '';
      }
      AuthService.logout();
      $window.localStorage.removeItem('aguagente-client-app');
      $window.localStorage.removeItem('aguagente-client-app-remember');
      $state.go('app.guest.login', {}, { reload: true });

    });

  })

  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {

    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.timeout = 600000;

    $ionicConfigProvider.backButton.previousTitleText(false).text('');
    $ionicConfigProvider.navBar.alignTitle('center');
    //$httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
    //$httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = 'http://localhost:8080';
    //$ionicConfigProvider.scrolling.jsScrolling(true);
    var APP_VERSION = "2.2.3";
    $httpProvider.interceptors.push(function ($q, $rootScope, $injector, API, $window) {

      return {
        request: function (config) {
          if ($rootScope.isJwt) {
            config.headers.Authorization = 'Bearer ' + $window.localStorage.getItem('access_token');
          }

          config.params = config.params || {};
          if (config.url.indexOf('auth/token') === -1) {
            $rootScope.$broadcast('loading:show');
            if (config.method.toLowerCase() == 'post') {
              config.params.appVersion = APP_VERSION;
              config.params.isMovilApp = "movil";
            }
          } else {
            config.params.appVersion = APP_VERSION;
            config.params.isMovilApp = "movil";
          }
          return config;
        },
        response: function (response) {
          $rootScope.$broadcast('loading:hide');
          return response;
        },
        requestError: function (err) {
          $rootScope.$broadcast('loading:hide');
          return err;
        },
        responseError: function (response) {
          console.log(response);
          if (response.status === 401 && (response.config.url.indexOf('auth/token') === -1 || response.config.url.indexOf('auth/refresh') === -1)) {
            if ($rootScope.isJwt) {
              if (response.data == 'token_expired') {
                var deferred = $q.defer();
                $injector.get('$http').post(API.URL + '/auth/refresh', {}, {
                  headers: {
                    Authorization: 'Bearer ' + $window.localStorage.getItem('access_token')
                  }
                }).then(function (responseRefresh) {
                  // If this request was successful, we will have a new
                  // token, so let's put it in storage
                  $window.localStorage.setItem('access_token', responseRefresh.data.token);
                  $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + responseRefresh.data.token;
                  //$http.defaults.headers.common['Authorization'] = 'Bearer ' + responseRefresh.data.token;
                  //$httpProvider.defaults.
                  // Now let's send the original request again
                  $injector.get('$http')(response.config)
                    .then(function (responseNew) {

                      // The repeated request was successful! So let's put
                      // this response back to the original workflow
                      return deferred.resolve(responseNew);
                    }, function () {
                      // Something went wrong with this request
                      // So we reject the response and carry on with 401
                      // error
                      //$rootScope.$broadcast('auth - logout');
                      console.log('error on injector 1');
                      return deferred.reject();
                    });
                }, function () {
                  // Refreshing the token failed, so let's carry on with
                  // 401
                  //$rootScope.$broadcast('auth - logout');
                  console.log('error on injector 2');
                  return deferred.reject();
                });
                return deferred.promise;
              }
            } else {
              $rootScope.$broadcast('token');
              $httpProvider.defaults.withCredentials = true;
            }
          } else if (response.status != 0 && response.status != 401 && response.config.url.indexOf('login') === -1) {
            if (response.status == 400) {
              if (response.data == 'token_error' || response.data == "token_invalid") {
                $rootScope.$broadcast('logoutUser');
              } else {
                console.log(response)
                var err = '<ul>';
                for (var i in response.data.response.errors) {
                  err += '<li><strong> * ' + response.data.response.errors[i] + "</strong></li>";
                }
                err += '</ul>';
                $rootScope.errModalTxt = err;
              }

            } else {
              if (response.status == 404 && response.data && response.data.error == 'user_not_found') {
                $rootScope.$broadcast('logoutUser');
              } else {
                $rootScope.errModalTxt = typeof response.data !== 'string' ? JSON.stringify(response.data) : response.data;
                
                if ($rootScope.errModalTxt != '' && $rootScope.errModalTxt != 'null' && response.status != 406 && response.data != 'token_error') {
                  $rootScope.$broadcast('errorHttp:show');
                }
              }


            }

            if (response.status === 406) {
              alert('Existe una actualización disponible de la app, por favor descarguela para continuar usando la aplicación');
            }

          }

          $rootScope.$broadcast('loading:hide');
          return ($q.reject(response));
        }
      };
    });

    $stateProvider

      /*.state('login', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginController'
      })*/

      /*.state('login', {
             url: '/login',
             views: {
               'guestContent': {
                 templateUrl: 'app/login/login.html',
                 controller: 'LoginController'
               }
             }
           })*/

      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'app/menu/menu.html',
        controller: 'MainController'
      });

    $urlRouterProvider.otherwise('/app/guest/home');
  });
