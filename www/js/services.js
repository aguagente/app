angular
	.module('aguagenteclient-services', [])

	.service('Network', Network)
	.service('AuthService', AuthService)
	.service('SessionService', SessionService)
	.service('Categories', Categories)
	.service('Elements', Elements)
	.service('Contracts', Contracts)
	.service('Payment', Payment)
	.service('Poll', Poll)
	.service('Imagen', Imagen)
	.service('MediaGallery', MediaGallery)
	.service('Charge', Charge)
	.service('Notificacion', Notificacion)
	.service('Coupon', Coupon)
	.service('DashboardChart', DashboardChart);
