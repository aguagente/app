angular
	.module('aguagenteclient-filters', [])

	.filter('AsDate', AsDate)
	.filter('AsDateUnix', AsDateUnix)
	.filter('contactImage', contactImage)
	.filter('contactName', contactName)
	.filter('trustSrc', trustSrc)
	.filter('DiasMes', DiasMes)