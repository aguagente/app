function AsDateUnix(){
	return function (fecha) {
        if(!fecha) return '';
        if(fecha instanceof Date) return fecha;
        return new Date(fecha*1000);
    }
}