function contactName(){
	return function (contact) {
		if(angular.isObject(contact)){

			if(contact.displayName)
				return contact.displayName;
			else if(contact.name){

				if(angular.isObject(contact.name)){
					if(contact.name.formatted)
						return contact.name.formatted;
					else if(contact.name.givenName) 
						return contact.name.givenName;
				}
				else
					return contact.name;
			}
			
		}
		
        return 'N/A';

    };
}
contactName.$inject = [];