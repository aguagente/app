function AsDate(){
	return function (fecha) {
        if(!fecha) return '';
        if(fecha instanceof Date) return fecha;
        var t = fecha.split(/[- :]/);
        return new Date( parseInt(t[0]), parseInt(t[1]) - 1, parseInt(t[2]), parseInt(t[3]) || 0, parseInt(t[4]) || 0, parseInt(t[5]) || 0);
    }
}