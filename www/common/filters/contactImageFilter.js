function contactImage(){
	return function (contact) {
		if(angular.isObject(contact)){

			if( angular.isDefined(contact.photos) && contact.photos && contact.photos.length){
				return contact.photos[0].type == 'url' ? 
							contact.photos[0].value : 
							contact.photos[0].value;
			}
			else if(contact.picture)
				return contact.picture;

		}

        return 'img/silueta.jpg';

    };
}
contactImage.$inject = [];