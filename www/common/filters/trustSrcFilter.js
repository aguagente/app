function trustSrc($sce){
	return function (src) {
        return $sce.trustAsResourceUrl(src);
    };
}
trustSrc.$inject = ['$sce'];