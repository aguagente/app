function Categories ($resource, API, $q) {

	this.database = null;
	this.categories = [];

	var resource = $resource(
		API.URL+'/categories/:id', 
		{
			id: '@id_categories'
		},
		{
			'get': { method:'GET', cache: true}
		}
	);

	return  {
		getAll: function(params) {

			var _this = this;

			return resource
				.get(params)
				.$promise
				.then(function (response) {
					if (_this.database) {
						var db = _this.database;
						db.transaction(function(tx) {

							tx.executeSql('DROP TABLE IF EXISTS categories');
							tx.executeSql("CREATE TABLE IF NOT EXISTS categories (id_categories INT PRIMARY KEY NOT NULL, name VARCHAR NOT NULL, description TEXT NOT NULL, elements TEXT)");
							angular.forEach(response.response, function(value,key){
								var query = "INSERT INTO categories (id_categories, name, description, elements) VALUES (?,?,?,?)";
								tx.executeSql( query, [value.id_categories, value.name, value.description, angular.toJson(value.elements) ] );
							});

						}, function(error) {
							console.log('Categories transaction error: ' + error.message);
						}, function() {
							console.log('Categories transaction ok');
						});
					}

					_this.categories = response.response;
					return _this.categories;

				}, function(err){
					console.log('entro');
					return _this.categories;

				});

		},

		setCategories : function(x){
			this.categories = x;
		},

		setDataBase : function(x){
			this.database = x;
		}

	}

}

Categories.$inject = ['$resource', 'API', '$q'];