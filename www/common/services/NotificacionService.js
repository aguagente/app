function Notificacion ($resource, $q, API) {

	var resource = $resource(
		API.URL+'/notifications/:id_notification', 
		{
			id_notification: '@id_notification'
		},
		{	
			'get': { 
				method:'GET'
			},
			report: {
				method : 'POST',
				url : API.URL+'/notifications/report'
			},
		}
	);

	return  {

		get : function(params){

			return resource
				.get(params)
				.$promise
				.then(function (response) {
					return response.response;
				});

		},

		report: function(params){
			
			return resource
				.report(params)
				.$promise
				.then(function(response){
					return response.response;
				});
		}

	}

}

Notificacion.$inject = ['$resource', '$q', 'API'];