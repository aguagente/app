function MediaGallery ($resource, $q, API, SessionService) {

	var resource = $resource(
		API.URL+'/posts/:id_post', 
		{
			id_post: '@id_post'
		},
		{	
			'get': { 
				method:'GET'
			}
		}
	);

	return  {

		get : function(params){

			return resource
					.get(params)
					.$promise
					.then(function (response) {
						return response.response;
					});

		},

		getPost: function(params) {
			return resource
					.get(params)
					.then(function(response) {
						return response;
					});

		}

	}

}

MediaGallery.$inject = ['$resource', '$q', 'API', 'SessionService'];