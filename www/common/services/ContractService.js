function Contracts($resource, $q, $filter, API, SessionService, Network) {

  this.database = null;
  this.contracts = [];

  var resource = $resource(
    API.URL + '/contracts/:id', {
      id: '@id_contracts'
    }, {

      update: {
        method: 'POST',
        headers: {
          'Content-Type': undefined,
          //'Content-Type': 'application/x-www-form-urlencoded'
        },
        transformRequest: function (data) {

          var fotos = [
            "proof_of_address",
            "profile",
            "id",
            "id_reverse",
            "image0",
            "image1",
            "image2",
            "image3",
            "image4",
            "image5"
          ];

          var formData = new FormData();

          for (var key in data) {
            if (key == 'upload_documents') {
              for (var index in data[key]) {
                var blob = data[key][index];
                if (blob instanceof Object) {
                  var imageName = index + '.' + (blob.mime.replace('image/', ''));
                  formData.append(index, blob.file, imageName);
                }
              }

            } else if (key == 'upload_photos') {

              for (var index in data[key]) {
                var blob = data[key][index];
                var imageName = index + '.' + (blob.mime.replace('image/', ''));
                formData.append("photos[]", blob.file, imageName);
              }

            } else if (key == 'elements') {
              for (var index in data[key]) {
                if (data[key][index]) {
                  formData.append("elements[]", data[key][index].id_categories_elements);
                }
              }

            } else if (key == 'material') {
              formData.append("elements[]", data[key].id_categories_elements);
            } else if (fotos.indexOf(key) === -1 && key != 'client') {
              formData.append(key, data[key]);
            } else if (key == 'client') {
              for (var index in data['client']) {
                formData.append('client[' + index + ']', data['client'][index]);
              }
            }
          }

          return formData;
        }
      },

      send: {
        method: 'POST',
        headers: {
          'Content-Type': undefined
        }
      },

      getByClient: {
        method: 'GET',
        url: API.URL + '/contracts?id_clients=:id'
      },

      getInvalidations: {
        method: 'GET',
        url: API.URL + '/invalidations?id_clients=:id'
      }

    }
  );

  return {

    setDataBase: function (x) {
      this.database = x;
    },

    setContracts: function (x) {
      this.contracts = x;
    },

    getByClient: function (id_client) {
      return resource
        .getByClient({"id": id_client})
        .$promise
        .then(function (response) {
          return response.response;
        });
    },

    getCountAll: function () {
      return this.contracts ? $filter('filter')(this.contracts, {'seller': SessionService.getIdClient()}).length : 0;
    },

    getAll: function (x) {
      return this.contracts;
    },

    getInvalidations: function (x) {
      return resource
        .getInvalidations({"id": x})
        .$promise
        .then(function (response) {
          return response.response;
        });
    },

    append: function (params) {

      var db = this.database;
      if (db) {
        var query = "INSERT INTO contracts (id_contrato, cliente, contrato) VALUES (?,?,?)";
        params.id_contrato = this.generateId();
        params.seller = SessionService.getIdClient();

        this.contracts.push(params);

        db.transaction(function (tx) {
          tx.executeSql(query, [params.id_contrato, params.seller, angular.toJson(params)], function (tx, res) {
            console.log("Insercion " + res.insertId);
          });
        }, function (error) {
          console.log('transaction error: ' + error.message);
        }, function () {
          console.log('transaction ok');
        });
      } else {
        params.id_contrato = this.generateId();
        //params.seller = SessionService.getIdClient();

        this.contracts.push(params);
      }

    },

    update: function (params) {

      var db = this.database;
      var query = "UPDATE contracts SET cliente = ?, contrato = ? WHERE id_contrato = ?";

      //this.contracts.push(params);

      db.transaction(function (tx) {
        tx.executeSql(query, [params.seller, angular.toJson(params), params.id_contrato], function (tx, res) {
          console.log("Updated " + params.id_contrato);
        });
      }, function (error) {
        console.log('transaction error: ' + error.message);
      }, function () {
        console.log('transaction ok');
      });

    },

    send: function (payload) {
      return resource
        .send(payload)
        .$promise
        .then(function (response) {
          return response.response;
        });
    },

    completar: function (payload, id) {
      return resource
        .send(payload, {'id_contracts': id})
        .$promise
        .then(function (response) {
          return response.response;
        });
    },

    toCorrect: function (payload) {
      return resource
        .update(payload)
        .$promise
        .then(function (response) {
          return response.response;
        });
    },

    delete: function (id) {
      var query = "DELETE FROM contracts WHERE id_contrato = ?";
      var id_contrato = this.contracts[id].id_contrato;
      var db = this.database;
      this.contracts.splice(id, 1);

      db.transaction(function (tx) {
        tx.executeSql(query, [id_contrato], function (tx, res) {
          console.log("Eliminado " + id_contrato);
        });
      }, function (error) {
        console.log('transaction error: ' + error.message);
      }, function () {
        console.log('transaction ok');
      });
    },

    reset: function () {
      this.contracts = [];
      var query = "DELETE FROM contracts";
      var db = this.database;
      db.executeSql(query, [], function (res) {
        console.log("Eliminados");
      }, function (err) {
        console.error(err);
      });
    },

    generateId: function () {
      return new Date().valueOf();
    }

  }

}

Contracts.$inject = ['$resource', '$q', '$filter', 'API', 'SessionService'];
