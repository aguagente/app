var AuthService = function ($http, $q, $filter, $timeout, SessionService, API, Categories, Elements) {

  var authService = {};
  var pushToken = null;

  authService.login = function (credentials, facebook) {

    var defer = $q.defer();
    var _this = this;

    $http
      .post(
        API.URL + '/auth/' + (facebook ? 'login-facebook' : 'login'),
        credentials,
        {withCredentials: true}
      )
      .then(function (response) {
        var data = response.data;
        var role = $filter('filter')(data.user.roles, {'name': 'Client'});
        if (!role.length) {
          SessionService.destroy();
          defer.reject('No tiene permisos de cliente');
        } else {
          if (API.isJwt) {
            SessionService.setAccessToken(data.access_token);
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + data.access_token;
          }
          var promises = [];
          promises.push($http.get(API.URL + '/clients?id_users=' + data.user.id));
          if (pushToken)
            promises.push($http.post(API.URL + '/users/registerDevice', {
              registration_id: pushToken,
              platform: (ionic.Platform.isIOS() ? 'ios' : 'android')
            }));
          promises.push(Categories.getAll());
          //promises.push( Elements.getAll() );

          $q.all(promises).then(function (results) {

            data.user.client = results[0].data.response[0];
            SessionService.setRemember(credentials.remember);
            SessionService.setUser(data.user);
            defer.resolve(SessionService.getUser());

          });

        }
      }, function (err) {
        if (err.status == 401) {
          defer.reject('Usuario y/o password incorrectos');
        } else {
          if (err.status != 406) {
            if (!API.isJwt) {
              authService.getToken();
            }
            defer.reject('Error al intentar iniciar sesión, por favor vuelve a intentar');
          } else {
            alert('Existe una actualización disponible de la app, por favor descarguela para continuar usando la aplicación');
          }
        }
      });
    return defer.promise;
  };

  authService.forget = function (email) {
    var defer = $q.defer();
    $http
      .post(
        API.URL + '/auth/email', {'email': email}
      )
      .then(function (response) {
        defer.resolve(response);
      }, function (response) {
        defer.reject(response);
      });
    return defer.promise;
  };

  authService.getUser = function (id) {
    var defer = $q.defer();
    $http
      .get(
        API.URL + '/users/show/' + id,
      )
      .then(function (response) {
        defer.resolve(response);
      }, function (response) {
        defer.reject(response);
      });
    return defer.promise;
  };

  authService.isRemember = function () {
    var remember = SessionService.getRemember();
    return remember == "true" || remember == true;
  };

  authService.getClientByEmail = function (email) {
    var defer = $q.defer();
    $http
      .get(API.URL + '/clients?email=' + email)
      .then(function (response) {
        defer.resolve(response.data);
      }, function (err) {
        defer.reject(err);
      });
    return defer.promise;
  };

  authService.getClientById = function (id) {
    var defer = $q.defer();
    $http
      .get(API.URL + '/clients?id_users=' + id)
      .then(function (response) {
        defer.resolve(response.data);
      }, function (err) {
        defer.reject(err);
      });
    return defer.promise;
  };

  authService.unregistered = function (payload) {
    var defer = $q.defer();
    $http
      .post(
        API.URL + '/auth/unregistered',
        payload
      )
      .then(function (response) {
        defer.resolve(response);
      }, function (response) {
        defer.reject(response);
      });
    return defer.promise;
  };

  authService.getToken = function () {
    var _this = this;
    console.log('withCredentials');
    return $http
      .get(API.URL + '/auth/token', {withCredentials: true})
      .then(function (response) {
        if (!API.isJwt) {
          $http.defaults.headers.common['X-CSRF-TOKEN'] = response.data.token;
        }
      }, function (response) {
        if (response.status != 406) {
          if (!API.isJwt) {
            $timeout(function () {
              authService.getToken();
            }, 5000);
          }
        }
      });
  };

  authService.refreshToken = function () {
    var _this = this;
    return $http
      .post(API.URL + '/auth/refresh')
      .then(function (response) {
        console.log(response);
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.access_token;
        //$http.defaults.headers.common['X-CSRF-TOKEN'] = response.data.token;
      }, function (response) {
      });
  };

  authService.isAuthenticated = function () {
    var user = SessionService.getUser();
    return angular.isObject(user);
  };

  authService.isAuthenticatedGuest = function () {
    var guest = SessionService.getGuest();
    return angular.isObject(guest);
  };

  authService.logout = function () {

    var defer = $q.defer();
    $http
      .get(API.URL + '/auth/logout')
      .then(function (response) {
        var res = response.data;
        SessionService.destroy();
        defer.resolve(res.user);

      }, function (response) {

        SessionService.destroy();
        defer.reject(response);

      });
    return defer.promise;

  };

  authService.setPushToken = function (token) {
    pushToken = token;
  };

  authService.getPushToken = function () {
    return pushToken;
  };

  authService.checkAppVersion = function () {
    return $http.post(API.URL + '/check_app_version', {});
  };

  return authService;
};

AuthService.$inject = ['$http', '$q', '$filter', '$timeout', 'SessionService', 'API', 'Categories', 'Elements'];
