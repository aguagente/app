function Elements ($resource, API, $q, $rootScope) {

	this.database = null;
	this.elements = [];

	var resource = $resource(
		API.URL+'/elements/:id', 
		{
			id: '@id_categories_elements'
		},
		{}
	);

	return  {
		getAll: function(params) {

			var _this = this;

			return resource
				.get(params)
				.$promise
				.then(function (response) {

					var db = _this.database;
					db.transaction(function(tx) {

						tx.executeSql('DROP TABLE IF EXISTS categories_elements');
						tx.executeSql("CREATE TABLE categories_elements (id_categories_elements INT PRIMARY KEY NOT NULL, id_categories INT NOT NULL, name VARCHAR NOT NULL, price REAL NOT NULL, image TEXT, type VARCHAR DEFAULT NULL)");
						angular.forEach(response.response, function(value,key){
							var query = "INSERT INTO categories_elements (id_categories_elements, id_categories, name, price, image, type) VALUES (?,?,?,?,?,?)";
							tx.executeSql( query, [value.id_categories_elements, value.id_categories, value.name, value.price, value.image, value.type] );
						});

					}, function(error) {
						console.log('Elements transaction error: ' + error.message);
					}, function() {
						console.log('Elements transaction ok');
					});

					_this.elements = response.response;
					return _this.elements;

				}, function(err){

					return _this.elements;
					
				});

		},

		setElements : function(x){
			this.elements = x;
		},

		setDataBase : function(x){
			this.database = x;
		}

	}

}

Elements.$inject = ['$resource', 'API', '$q'];