function Coupon ($resource, API) {

	var resource = $resource(
		API.URL+'/coupon', 
		{},
		{	
			'get': { 
				method:'GET'
			}
		}
	);

	return  {

		get: function(params){

			return resource
					.get(params)
					.$promise
					.then(function (response) {
						return response.response;
					});
		},

		getDiscount: function(level) {
			var _return = 0;
			switch(level){
				case 1:
					_return = 100; 
					break;
				case 2:
					_return = 100;
					break;
				case 3:
					_return = 200;
					break;
				case 4:
					_return = 499;
					break;
				case 5:
					_return = 799;
					break;
				default:
					_return = 999;
					break;
			}

			return _return;
		}
	}
}

Coupon.$inject = ['$resource', 'API'];