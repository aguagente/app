function Network () {


	return  {

		isOnline : function(){
			var nav = navigator;
			var connection = nav.connection || nav.mozConnection || nav.webkitConnection;
			if(connection){
				return connection.type && connection.type != "none";
			}
			return nav.onLine;
		}

	}

}

Network.$inject = [];