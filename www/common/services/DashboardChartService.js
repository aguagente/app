function DashboardChart($resource, API, $q) {

  this.database = null;
  this.categories = [];

  var resource = $resource(
    API.URL + '/dashboard_charts/:id',
    {
      id: '@id_dashboard_chart'
    }, {
      create: {
        method: 'POST'
      }
    }
  );

  return {
    getAll: function (params) {

      var _this = this;

      return resource
        .get(params)
        .$promise
        .then(function (response) {
          return response;

        }, function (err) {
          return _this.categories;

        });

    },
    create: function (data) {
      return resource
        .create(data)
        .$promise
        .then(function (response) {
          return response.response;
        });
    }
  }

}

DashboardChart.$inject = ['$resource', 'API', '$q'];
