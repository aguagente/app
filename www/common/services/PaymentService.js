function Payment ($resource, $q, API, SessionService) {

	var resource = $resource(
		API.URL+'/clients/:id_clients/cards',
		{
			id_clients: '@id_clients'
		},
		{

			getCard: {
				method: 'GET',
				url : API.URL + '/clients/:id_clients/cards/:id_card'
			},
			delete : {
				method : 'DELETE',
				url : API.URL + '/clients/:id_clients/cards/:id_card'
			},
			setAsDefault : {
				method : 'POST',
				url : API.URL + '/clients/:id_clients/cards/:id_card/setAsDefault'
			},
			getFees: {
				method: 'GET',
				url : API.URL + '/fees'
			}

		}
	);

	return  {

		getAll: function() {
			return resource
					.get({"id_clients":SessionService.getIdClient()})
					.$promise
					.then(
						function(response){
							return response.response;
						},
						function(err){
							console.log(err);
						}
					);
		},

		create : function(token, deviceSessionId = null){
			return resource
				   .save({"id_clients":SessionService.getIdClient(),"token" : token, deviceSessionId: deviceSessionId})
				   .$promise
				   .then(function(response){
				   		return response;
				   });
		},

		delete : function(id_card){
			return resource
					.delete({"id_clients":SessionService.getIdClient(),"id_card":id_card})
					.$promise
					.then(function(response){
						return response;
					});
		},

		setAsDefault : function(id_card){
			return resource
					.setAsDefault({"id_clients":SessionService.getIdClient(),"id_card":id_card} , null)
					.$promise
					.then(function(response){
						return response;
					});
		},

		getFees : function(){
			return resource
					.getFees()
					.$promise
					.then(function(response){
						return response.response;
					});
		}

	}

}

Payment.$inject = ['$resource', '$q', 'API', 'SessionService'];
