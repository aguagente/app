function Poll ($resource, $q, API, SessionService, Network) {

	var resource = $resource(
		API.URL+'/polls/:id_polls', 
		{
			id_polls: '@id_polls'
		},
		{
			getByClient: {
				method : 'GET',
				url : API.URL+'/clients/:id_client/missingPolls'
			},

			answer : {
				method : 'POST',
				url : API.URL+'/polls/:id_polls/answer'
			}

		}
	);

	return  {

		getByClient : function(){
			return resource
				.getByClient({"id_client": SessionService.getIdClient()})
				.$promise
				.then(function (response) {
					return response.response;
				});
		},

		getAll: function(x) {
			return resource
				.get()
				.$promise
				.then(function (response) {
					return response.response;
				});
		},

		show : function(id){
			return resource
				.get({id_polls:id})
				.$promise
				.then(function (response) {
					return response.response;
				});
		},

		answer : function(id,payload){
			return resource
				.answer({"id_polls":id},payload)
				.$promise
				.then(function (response) {
					return response.response;
				});
		}

	}

}

Poll.$inject = ['$resource', '$q', 'API', 'SessionService'];