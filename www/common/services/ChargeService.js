function Charge ($resource, $q, $state, API, AuthService) {

	var resource = $resource(
		API.URL+'/charges/:id_charges', 
		{
			id_charges: '@id_charges'
		},
		{	
			'get': { 
				method:'GET'
				//isArray:true
			}
		}
	);

	return  {

		get : function(params){

			return resource
				.get(params)
				.$promise
				.then(
					function (response) {
						return response.response;
					},
					function(err){
						if (err.status == 401) {
							AuthService.logout();
							$state.go('app.guest.login');
						}
					}
				);

		}

	}

}

Charge.$inject = ['$resource', '$q', '$state', 'API', 'AuthService'];