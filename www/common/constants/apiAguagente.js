var apiAguagente = {
  "URL": 'https://panelaguagente.xyz/api/public',
  //"URL": 'https://panelaguagente.xyz/demo/v3/api/public',
  //"URL": 'http://192.168.0.6/aguagente/public',
  //"FACE": 'http://tiny.cc/0pd10y' // RIGEL new
  //"FACE" : 'http://tiny.cc/p9f6fy'//OLD
  "FACE": "http://tiny.cc/s67l1y",
  //"PROFILE_IMAGES": '',
  "environment": 'production',
  "isJwt": true,
  "openPayEnvironment": 'sandbox'
};
